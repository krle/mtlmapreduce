source ~/.bash_profile

dfs -rm input
dfs -rm -r output*
dfs -put Resources/Traces-SnL/traces/p10_400000.csv input
zip -d target/MTLMapReduce-0.0.1-SNAPSHOT-jar-with-dependencies.jar META-INF/LICENSE
hadoop target/MTLMapReduce-0.0.1-SNAPSHOT-jar-with-dependencies.jar it.polimi.krstic.MTLMapReduce.MTLHistoryCheck input Resources/Fmae-SnL/P10 output

