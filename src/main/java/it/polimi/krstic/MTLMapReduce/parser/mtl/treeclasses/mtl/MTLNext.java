/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.Next;
import it.polimi.krstic.MTLMapReduce.utils.Printer;

/**
 * @author krle
 *
 */
public class MTLNext extends Next {

	private Interval I;
	public MTLNext(Interval i, MTLFormula fma) {
		super(fma);
		I=i;
		
		range=Interval.union(Interval.includeZero(I), Interval.project(I, getSub().getRangeInterval()));	

	}
	
	@Override
	public String print(Printer p){
		String s="";
		
		s+=OP;
		s+=I.print();
		for (MTLFormula mtlFormula : subformulae) {
			s+=mtlFormula.print(p);
		}
		
		return p.print(s);

	}
	/**
	 * @return the i
	 */
	public Interval getInterval() {
		return I;
	}
}
