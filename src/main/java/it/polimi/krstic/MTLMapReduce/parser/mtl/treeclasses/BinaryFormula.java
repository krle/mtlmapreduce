package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses;

import it.polimi.krstic.MTLMapReduce.utils.Printer;


public class BinaryFormula extends MTLFormula {

	public BinaryFormula(MTLFormula lfma, MTLFormula rfma){
		super(lfma,rfma);
	}

	
	/**
	 * @return the left subformula
	 */
	public MTLFormula getLeftSub() {
		return subformulae.get(0);
	}
	
	/**
	 * @return the right subformula
	 */
	public MTLFormula getRightSub() {
		return subformulae.get(1);
	}
	
	public String print(Printer p){
		String s="";
		
		s+=getLeftSub().print(p);
		s+=OP;
		s+=getRightSub().print(p);
		
		return p.print(s);

	}
	
}


