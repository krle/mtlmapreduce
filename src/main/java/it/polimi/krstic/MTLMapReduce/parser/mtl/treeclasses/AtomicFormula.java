/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses;

import it.polimi.krstic.MTLMapReduce.utils.Printer;

/**
 * @author krle
 *
 */
public class AtomicFormula extends MTLFormula {
	
	
	private String atom;
	public AtomicFormula(String prop){
		super();
		atom=prop;
		range=new BoundedInterval(0,0);
	}
	/**
	 * @return the atom
	 */
	public String getAtom() {
		return atom;
	}
	
	@Override
	public String print(Printer p) {
		p.print(atom);
		return atom;
	}
	

}
