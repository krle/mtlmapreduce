/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.UnaryFormula;

/**
 * @author krle
 *
 */
public class Yesterday extends UnaryFormula {

	public Yesterday(MTLFormula fma) {
		super(fma);
		OP=MTLFormula.YESTERDAY;
		range = new Interval(0);
	}

}
