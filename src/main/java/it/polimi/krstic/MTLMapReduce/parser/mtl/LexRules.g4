lexer grammar LexRules; 

IDENTIFIER : [a-zA-Z_][a-zA-Z0-9_]* ;	//match identifiers
NUMBER : [0-9]+ ;	//match positive integers
WS : [ \t\n]+ -> skip ;	//toss out whitespace
BLOCK_COMMENT : '/*' .*? '*/' -> channel(HIDDEN);	//hide block comments
LINE_COMMENT : '//' ~[\r\n]* -> channel(HIDDEN);	//hide line comments


