/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses;



/**
 * @author krle
 *
 */
public class UnaryFormula extends MTLFormula {
	
	
	public UnaryFormula(MTLFormula fma){
		super(fma);
	}
	
	/**
	 * @return the subformula
	 */
	public MTLFormula getSub() {
		return subformulae.get(0);
	}
	

}
