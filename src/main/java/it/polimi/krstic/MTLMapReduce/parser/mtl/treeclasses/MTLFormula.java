package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.bool.Or;
import it.polimi.krstic.MTLMapReduce.utils.Printer;
import it.polimi.krstic.MTLMapReduce.utils.StringPrinter;

import java.util.ArrayList;


public class MTLFormula{
	
	/**
	 * Constants
	 */
	public static final String AND_OP = "&";
	public static final String OR_OP = "|";
	public static final String NOT_OP = "!";
	public static final String IMPL = "->";
	public static final String EQUV = "<->" ;
	public static final String UNTIL = "-U-" ;
	public static final String RELEASE = "-R-" ;
	public static final String GLOBALLY = "-G-";
	public static final String NEXT = "-X-";
	public static final String FUTURE = "-F-";
	public static final String SINCE = "-S-" ;
	public static final String TRIGGER = "-T-";
	public static final String HISTORICALLY = "-H-";
	public static final String YESTERDAY = "-Y-";
	public static final String PAST = "-P-";
	
	/**
	 * List of parents
	 */
	protected ArrayList<MTLFormula> superformulae = new ArrayList<MTLFormula>();
	/**
	 * List of children
	 */
	protected ArrayList<MTLFormula> subformulae = new ArrayList<MTLFormula>();

	
	protected Interval range;
	
	
	protected String OP = "";

	
	/**
	 * Constructor for formulae that do not have children (Atomic formulae, or perhaps FO predicates)
	 */
	public MTLFormula(){
		
	}
	
	/**
	 * @param child - subformula of this
	 * NOTE: Constructor used by unary formulae
	 */
	public MTLFormula(MTLFormula child){
		subformulae.add(child);
		child.addParent(this);
	}
	
	/**
	 * @param child - two subformulae of this
	 * NOTE: Constructor used by binary formulae
	 */
	public MTLFormula(MTLFormula leftchild,MTLFormula rightchild){
		subformulae.add(leftchild);
		subformulae.add(rightchild);

		leftchild.addParent(this);
		rightchild.addParent(this);
	}
	/**
	 * @param children - subformulae of this
	 * NOTE: this will update the parent formulae of the specified children
	 */
	public MTLFormula(ArrayList<MTLFormula> children){
		subformulae=children;
		for (MTLFormula mtlFormula : subformulae) {
			mtlFormula.addParent(this);
		}
	}
	
	/**
	 * @param parent - parent of this
	 * Method adds a parent to the list of parents of this
	 */
	private void addParent(MTLFormula parent) {
		superformulae.add(parent);	
	}

	/**
	 * @return true if formula has no parent, otherwise false
	 */
	public boolean isRoot(){
		return superformulae.isEmpty();
	}
	
	/**
	 * @return true if formula has no child, otherwise false
	 */
	public boolean isLeaf(){
		return subformulae.isEmpty();
	}
	
	
	/**
	 * @return the height of this
	 */
	public int height() {
		int max=0;
		for (MTLFormula mtlFormula : subformulae) {
			max=Math.max(mtlFormula.height(), max);
		}
		return max+1;
	}
	
	
	public boolean atomic(){
		return isLeaf();
	}
	/**
	 * @return true if this is a boolean formula (!, &, |, ->, <->) 
	 */
	public boolean bool(){
		return false;
	}
	/**
	 * @return true if this is a temporal formula, i.e., 
	 * -U-,-S-,-T-,-R-,-G-,-F-,-H-,-P-,-X-,-Y-
	 */
	public boolean temporal(){
		return !bool()&&!atomic();
	}
	
	/**
	 * @return true if this is a formula whose evaluation 
	 * would require hybrid semantics, i.e., a temporal formula with
	 * a boolean combination of temporal-only subformulae 
	 */
	public boolean hybrid(){
		return temporal() && subhybrid();
	}
	
	/**
	 * @return true if this has subformulae that are temporal or a boolean 
	 * combination of temporal-only subformulae 
	 */
	public boolean subhybrid(){
	
		boolean cond=!atomic();
		for (MTLFormula mtlFormula : subformulae) {
			if(!mtlFormula.temporal()) {
				cond = cond && mtlFormula.subhybrid();
			}
		}
		return cond;
	
	}
	
	public boolean future() {
		return false;
	}
	
	/**
	 * @param p - specifies the method of printing (i.e. an implementation of the Printer interface)
	 * 
	 */
	public String print(Printer p){
		String s="";
		
		s+=OP;
		for (MTLFormula mtlFormula : subformulae) {
			s+=mtlFormula.print(p);
		}
		
		return p.print(s);

	}
	
	public String key(){
		return print(new StringPrinter());
	}

	public MTLFormula expandFormula(int K) {
		
		ArrayList<MTLFormula> children = subformulae;
		for (MTLFormula mtlFormula : subformulae) {
			children.add(mtlFormula.expandFormula(K));
		}
		
		subformulae=children;
		return this;
	}
	
	
	public MTLFormula lazy2pointwise(Or phiact) {
		
		ArrayList<MTLFormula> children = subformulae;
		for (MTLFormula mtlFormula : subformulae) {
			children.add(mtlFormula.lazy2pointwise(phiact));
		}
		
		subformulae=children;
		return this;
	}
	

	/**
	 * @return the superformulae
	 */
	public ArrayList<MTLFormula> getParents() {
		return new ArrayList<MTLFormula>(superformulae);
	}
	
	
	public Interval getRangeInterval() {
		if(range instanceof BoundedInterval){
			BoundedInterval bi = (BoundedInterval) range;
			return new BoundedInterval(bi.A, bi.B);
		}
		else{
			return new Interval(range.A);
		}
	}

	

}
