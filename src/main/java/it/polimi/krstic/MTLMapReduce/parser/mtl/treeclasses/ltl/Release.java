package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BinaryFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;

public class Release extends BinaryFormula {
	public Release(MTLFormula lfma, MTLFormula rfma) {
		super(lfma, rfma);
		OP=MTLFormula.RELEASE;
		range = new Interval(0);
	}
	
	/**
	 * @return true if this has subformulae that are temporal or a boolean 
	 * combination of temporal-only subformulae 
	 */
	
	@Override
	public boolean subhybrid(){
		return getRightSub().subhybrid();
	}
	
	@Override
	public boolean future() {
		return true;
	}
}
