/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BinaryFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;

/**
 * @author krle
 *
 */
public class Since extends BinaryFormula {
	public Since(MTLFormula lfma, MTLFormula rfma) {
		super(lfma, rfma);
		OP=MTLFormula.SINCE;
		range = new Interval(0);
	}
	@Override
	public boolean subhybrid(){
		return getRightSub().subhybrid();
	}
}
