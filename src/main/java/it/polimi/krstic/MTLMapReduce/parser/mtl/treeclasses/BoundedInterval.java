/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses;

/**
 * @author krle
 *
 */
public class BoundedInterval extends Interval{
	public BoundedInterval(long a, long b) {
		super(a);
		B=b;
		infinite=false;
	}
	public long B;
	
	@Override
	public String print() {
		return "["+A+","+B+"]";
	}
}

