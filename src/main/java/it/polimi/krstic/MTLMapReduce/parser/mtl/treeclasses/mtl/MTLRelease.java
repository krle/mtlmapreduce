package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.Release;
import it.polimi.krstic.MTLMapReduce.utils.Printer;

public class MTLRelease extends Release {
	private Interval I;
	public MTLRelease(Interval i, MTLFormula lfma, MTLFormula rfma) {
		super(lfma, rfma);
		I=i;
		
		Interval withZero = Interval.includeZero(I);
		range=Interval.union(withZero, 
					   Interval.union(Interval.project(withZero, getLeftSub().getRangeInterval()), 
							   		  Interval.project(I, getRightSub().getRangeInterval())));
	}
	
	@Override
	public String print(Printer p){
		String s="";
		
		s+=getLeftSub().print(p);
		s+=OP;
		s+=I.print();
		s+=getRightSub().print(p);
		
		return p.print(s);

	}
	/**
	 * @return the i
	 */
	public Interval getInterval() {
		return I;
	}
}
