/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses;


import it.polimi.krstic.MTLMapReduce.utils.Printer;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author krle
 *
 */
public class NaryFormula extends MTLFormula {

	
	public NaryFormula(ArrayList<MTLFormula> fmas){
		super(fmas);
	}
	
	/**
	 * @return iterator with all subformulae
	 */
	public Iterator<MTLFormula> getSub() {
		return subformulae.iterator();
	}
	
	@Override
	public String print(Printer p){
		String s="";
		
		s+="(";
		Iterator<MTLFormula> iter = getSub();
		while (iter.hasNext()){
			s+=iter.next().print(p);
			if(iter.hasNext())
				s+=OP;
		}
		s+=")";
		
		return p.print(s);
	}
	
}


