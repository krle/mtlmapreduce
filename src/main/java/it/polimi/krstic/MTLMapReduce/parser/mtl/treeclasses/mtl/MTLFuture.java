/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl;

import java.util.ArrayList;


import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BoundedInterval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.bool.Or;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.Future;
import it.polimi.krstic.MTLMapReduce.utils.Printer;

/**
 * @author krle
 *
 */
public class MTLFuture extends Future {

	private Interval I;
	public MTLFuture(Interval i, MTLFormula fma) {
		super(fma);
		I=i;
		
		range=Interval.union(Interval.includeZero(I), Interval.project(I, getSub().getRangeInterval()));	
	}
	
	@Override
	public String print(Printer p){
		String s="";
		
		s+=OP;
		s+=I.print();
		for (MTLFormula mtlFormula : subformulae) {
			s+=mtlFormula.print(p);
		}
		
		return p.print(s);

	}
	/**
	 * @return the i
	 */
	public Interval getInterval() {
		return I;
	}
	
	@Override
	public MTLFormula expandFormula(int K) {
		//expand only bounded MTL formula  
		if(I instanceof BoundedInterval){
			BoundedInterval bI = (BoundedInterval)I;
			if(K>bI.B){
				return new MTLFuture(I, getExpandedSub(K));
			}
			else{
				if(K<=bI.A){
					bI.A-=K;
					bI.B-=K;
					MTLFormula sub = expandFormula(K);
					return new MTLFuture(new BoundedInterval(K, K),sub);
				}
				else{
					long a = bI.A; 
					bI.A=0;
					bI.B-=K;
					if(bI.B>0){
						MTLFormula sub = expandFormula(K);
						ArrayList<MTLFormula> orsubs = new ArrayList<MTLFormula>();
						orsubs.add(new MTLFuture(new BoundedInterval(a % K,K),getExpandedSub(K)));
						orsubs.add(new MTLFuture(new BoundedInterval(K, K), sub));
						return new Or(orsubs);
					}
					else
					{
						return new MTLFuture(new BoundedInterval(a % K,K),getExpandedSub(K));
					}
				}		
			}
		}
		else{
			return new MTLFuture(I, getExpandedSub(K));
		}
	}
	
	private MTLFormula expandedSub=null;
	private MTLFormula getExpandedSub(int K){
		if(expandedSub==null){
			expandedSub=getSub().expandFormula(K);
		}
		return expandedSub;
	}
		
		

}
