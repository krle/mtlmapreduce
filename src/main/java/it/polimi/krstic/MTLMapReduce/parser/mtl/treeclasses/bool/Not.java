/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.bool;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.UnaryFormula;

/**
 * @author krle
 *
 */
public class Not extends UnaryFormula {

	public Not(MTLFormula fma) {
		super(fma);
		OP = "!";
		range = getSub().getRangeInterval();
	}
	
	@Override
	public boolean bool() {
		return true;
	}

}
