package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.UnaryFormula;

public class Historically extends UnaryFormula {

	public Historically(MTLFormula fma) {
		super(fma);
		OP=MTLFormula.HISTORICALLY;	
		range = new Interval(0);
	}

}
