package it.polimi.krstic.MTLMapReduce.parser.mtl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.HashMap;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;

import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLGrammarParser.AndformulaContext;
import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLGrammarParser.FormulaContext;
import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLGrammarParser.OrformulaContext;
import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLGrammarParser.ProgramContext;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.bool.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.*;
import it.polimi.krstic.MTLMapReduce.utils.Printer;

public class MTLListener extends MTLGrammarBaseListener{

	

	//parsing utilities
	private Deque<MTLFormula> current = new ArrayDeque<MTLFormula>();
	private Deque<Integer> arity = new ArrayDeque<Integer>();
	
	//resulting structure
	MTLFormula formula=null;
	HashMap<String,MTLFormula> mapping = new HashMap<String,MTLFormula>();
	HashMap<String,AtomicFormula> atoms = new HashMap<String,AtomicFormula>();

	
	@Override
	public void exitProgram(ProgramContext ctx) {
		formula=current.pop();
		compactFormula();
	}

	
	@Override
	public void enterFormula(FormulaContext ctx) {
		int subtrees=ctx.getChildCount();
		if(subtrees==5){
			arity.push(1);
		}
	}
	
	@Override
	public void exitAndformula(AndformulaContext ctx) {
		arity.push(arity.pop()+1);
	}


	@Override
	public void exitOrformula(OrformulaContext ctx) {
		arity.push(arity.pop()+1);
	}
	
	@Override
	public void exitFormula(@NotNull FormulaContext ctx) {
		MTLFormula recognize = recognizeAndConstuct(ctx);
		mapping.put(recognize.key(), recognize);
		current.push(recognize);
	}
	
	

	/**
	 * @param ctx - context
	 * @return object corresponding to the parsed formula
	 */
	private MTLFormula recognizeAndConstuct(FormulaContext ctx) {
		int subtrees=ctx.getChildCount();
		switch(subtrees){
			case 1:{
				return parseOne(ctx);
			}
			case 2:{
				return parseTwo(ctx);
			}
			case 3:{
				return parseThree(ctx);
			}
			case 4:{
				return parseFour(ctx);
			}
			case 5:{
				return parseFive(ctx);
			}
			default :{
				return null;
			}
		}
	}



	private MTLFormula parseFive(FormulaContext ctx) {
		switch (ctx.getChild(2).getText()){
			case MTLFormula.AND_OP :{
				ArrayList<MTLFormula> subs = new ArrayList<MTLFormula>();
				int a = arity.pop();
				for (int i = 0; i < a; i++) {
					subs.add(current.pop());	
				}
			    Collections.reverse(subs);
				MTLFormula f = new And(subs);
				return f;
			}
			case MTLFormula.OR_OP :{
				ArrayList<MTLFormula> subs = new ArrayList<MTLFormula>();
				int a = arity.pop();
				for (int i = 0; i < a; i++) {
					subs.add(current.pop());	
				}
			    Collections.reverse(subs);
				MTLFormula f = new Or(subs);
				return f;
			}
			default :{
				return null;
			}
		}
	}



	private MTLFormula parseFour(FormulaContext ctx) {
		switch (ctx.getChild(1).getText()){
			case MTLFormula.UNTIL :{
				MTLFormula right = current.pop();
				return new MTLUntil(getInterval(ctx.getChild(2)),current.pop(),right);
			}
			case MTLFormula.RELEASE :{
				MTLFormula right = current.pop();
				return new MTLRelease(getInterval(ctx.getChild(2)),current.pop(),right);
			}
			case MTLFormula.SINCE :{
				MTLFormula right = current.pop();
				return new MTLSince(getInterval(ctx.getChild(2)),current.pop(),right);
			}
			case MTLFormula.TRIGGER :{
				MTLFormula right = current.pop();
				return new MTLTrigger(getInterval(ctx.getChild(2)),current.pop(),right);
			}
			default :{
				return null;
			}
		}
	}



	private MTLFormula parseThree(FormulaContext ctx) {
		switch (ctx.getChild(0).getText()){
			case "(" :{
				return current.pop();
			}
			case MTLFormula.GLOBALLY :{
				return new MTLGlobally(getInterval(ctx.getChild(1)), current.pop());
			}
			case MTLFormula.NEXT :{
				return new MTLNext(getInterval(ctx.getChild(1)),current.pop());
				
			}
			case MTLFormula.FUTURE :{
				return new MTLFuture(getInterval(ctx.getChild(1)),current.pop());
				
			}
			case MTLFormula.HISTORICALLY :{
				return new MTLHistorically(getInterval(ctx.getChild(1)),current.pop());
				
			}
			case MTLFormula.YESTERDAY :{
				return new MTLYesterday(getInterval(ctx.getChild(1)),current.pop());
				
			}
			case MTLFormula.PAST :{
				return new MTLPast(getInterval(ctx.getChild(1)),current.pop());
			}
			default :{
				switch (ctx.getChild(1).getText()){
					case MTLFormula.IMPL :{
						MTLFormula right = current.pop();
						return new Impl(current.pop(),right);
					}
					case MTLFormula.EQUV :{
						MTLFormula right = current.pop();
						return new Equiv(current.pop(),right);
					}
					case MTLFormula.UNTIL :{
						MTLFormula right = current.pop();
						return new Until(current.pop(),right);
					}
					case MTLFormula.RELEASE :{
						MTLFormula right = current.pop();
						return new Release(current.pop(),right);
					}
					case MTLFormula.SINCE :{
						MTLFormula right = current.pop();
						return new Since(current.pop(),right);
					}
					case MTLFormula.TRIGGER :{
						MTLFormula right = current.pop();
						return new Trigger(current.pop(),right);
					}
					default :{
						return null;
					}
					
				}
			}
	}
	}



	private Interval getInterval(ParseTree child) {
		switch (child.getChild(0).getText()){
			case "(":{
				return new BoundedInterval(Integer.parseInt(child.getChild(1).getText())+1,Integer.parseInt(child.getChild(3).getText())-1);
			}
			case "[":{
				return new BoundedInterval(Integer.parseInt(child.getChild(1).getText()),Integer.parseInt(child.getChild(3).getText()));
			}
			default :{
				return null;
			}
			
		}
	}



	private MTLFormula parseTwo(FormulaContext ctx) {
		switch (ctx.getChild(0).getText()){
			case MTLFormula.NOT_OP :{
				return new Not(current.pop());
			}
			case MTLFormula.GLOBALLY :{
				return new Globally(current.pop());
			}
			case MTLFormula.NEXT :{
				return new Next(current.pop());
				
			}
			case MTLFormula.FUTURE :{
				return new Future(current.pop());
				
			}
			case MTLFormula.HISTORICALLY :{
				return new Historically(current.pop());
				
			}
			case MTLFormula.YESTERDAY :{
				return new Yesterday(current.pop());
				
			}
			case MTLFormula.PAST :{
				return new Past(current.pop());
			}
			default :{
				return null;
			}
		}

	}



	private MTLFormula parseOne(FormulaContext ctx) {
		// TODO refactor such that already recognized formulae are returned
		if(atoms.containsKey(ctx.getChild(0).getText()))
			return atoms.get(ctx.getChild(0).getText());
		else {
			AtomicFormula a = new AtomicFormula(ctx.getChild(0).getText());	
			atoms.put(a.key(), a);
			return a;
		}
	}




	/* (non-Javadoc)
	 * @see it.polimi.krstic.MTLMapReduce.parser.Listenable#printTree()
	 */
	public String printLatest(Printer p) {
		return formula.print(p);		
	}
	
	private void compactFormula(){
		// TODO implement
	}
	
	public void expandFormula(int K){
		formula=formula.expandFormula(K);
	}

}
