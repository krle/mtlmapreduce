/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.bool;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BoundedInterval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.NaryFormula;

import java.util.ArrayList;

/**
 * @author krle
 *
 */
public class Or extends NaryFormula {
	
	public Or(ArrayList<MTLFormula> fmas){
		super(fmas);
		OP = "|";
		
		range = new BoundedInterval(0,0);
		
		for (MTLFormula mtlFormula : fmas) {
			range=Interval.union(range, mtlFormula.getRangeInterval());
		}
		
	}
	
	@Override
	public boolean bool() {
		return true;
	}
}
