grammar MTLGrammar;
import  MTLLexer,LexRules;


program : 	formula
;


formula : 	atomic 						
		| NOT_OP formula			
		| LPAR andformula AND_OP formula RPAR
		| LPAR orformula OR_OP formula RPAR
		| formula IMPL formula
		| formula EQUV formula
		| formula UNTIL formula
		| formula RELEASE formula
		| GLOBALLY formula
		| NEXT formula
		| FUTURE formula
		| formula SINCE formula
		| formula TRIGGER formula
		| HISTORICALLY formula
		| YESTERDAY formula
		| PAST formula
		| formula UNTIL interval formula
		| formula RELEASE interval formula
		| GLOBALLY interval formula
		| NEXT interval formula
		| FUTURE interval formula
		| formula SINCE interval formula
		| formula TRIGGER interval formula
		| HISTORICALLY interval formula
		| YESTERDAY interval formula
		| PAST interval formula
		| LPAR formula RPAR
;

andformula : formula
		| andformula AND_OP formula
;

orformula : formula
		| orformula OR_OP formula
;

atomic : IDENTIFIER			
;


interval : 	LPAR NUMBER COMMA NUMBER RPAR
		| LSQUARE NUMBER COMMA NUMBER RSQUARE
;



