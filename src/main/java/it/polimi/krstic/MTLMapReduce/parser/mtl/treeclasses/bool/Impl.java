/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.bool;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BinaryFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula; 

/**
 * @author krle
 *
 */
public class Impl extends BinaryFormula {

	public Impl(MTLFormula lfma, MTLFormula rfma) {
		super(lfma, rfma);
		OP="->";
		range=Interval.union(getLeftSub().getRangeInterval(),getRightSub().getRangeInterval());
	}
	
	@Override
	public boolean bool() {
		return true;
	}

}
