package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BinaryFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;

public class Trigger extends BinaryFormula {
	public Trigger(MTLFormula lfma, MTLFormula rfma) {
		super(lfma, rfma);
		OP=MTLFormula.TRIGGER;
		range = new Interval(0);
	}
	@Override
	public boolean subhybrid(){
		return getRightSub().subhybrid();
	}
}
