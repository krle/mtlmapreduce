package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.Historically;
import it.polimi.krstic.MTLMapReduce.utils.Printer;

public class MTLHistorically extends Historically {

	private Interval I;
	public MTLHistorically(Interval i, MTLFormula fma) {
		super(fma);
		I=i;
		
		Interval neg = Interval.negate(I);
		range=Interval.union(Interval.includeZero(neg), Interval.project(neg, getSub().getRangeInterval()));	
	}
	
	@Override
	public String print(Printer p){
		String s="";
		
		s+=OP;
		s+=I.print();
		for (MTLFormula mtlFormula : subformulae) {
			s+=mtlFormula.print(p);
		}
		
		return p.print(s);

	}
	/**
	 * @return the i
	 */
	public Interval getInterval() {
		return I;
	}
}
