/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.UnaryFormula;

/**
 * @author krle
 *
 */
public class Globally extends UnaryFormula {

	public Globally(MTLFormula fma) {
		super(fma);
		OP=MTLFormula.GLOBALLY;
		range = new Interval(0);
	}

	
	@Override
	public boolean future() {
		return true;
	}
}
