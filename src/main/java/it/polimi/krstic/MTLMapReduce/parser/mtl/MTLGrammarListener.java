// Generated from MTLGrammar.g4 by ANTLR 4.2.2
package it.polimi.krstic.MTLMapReduce.parser.mtl;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MTLGrammarParser}.
 */
public interface MTLGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MTLGrammarParser#interval}.
	 * @param ctx the parse tree
	 */
	void enterInterval(@NotNull MTLGrammarParser.IntervalContext ctx);
	/**
	 * Exit a parse tree produced by {@link MTLGrammarParser#interval}.
	 * @param ctx the parse tree
	 */
	void exitInterval(@NotNull MTLGrammarParser.IntervalContext ctx);

	/**
	 * Enter a parse tree produced by {@link MTLGrammarParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull MTLGrammarParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MTLGrammarParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull MTLGrammarParser.ProgramContext ctx);

	/**
	 * Enter a parse tree produced by {@link MTLGrammarParser#andformula}.
	 * @param ctx the parse tree
	 */
	void enterAndformula(@NotNull MTLGrammarParser.AndformulaContext ctx);
	/**
	 * Exit a parse tree produced by {@link MTLGrammarParser#andformula}.
	 * @param ctx the parse tree
	 */
	void exitAndformula(@NotNull MTLGrammarParser.AndformulaContext ctx);

	/**
	 * Enter a parse tree produced by {@link MTLGrammarParser#orformula}.
	 * @param ctx the parse tree
	 */
	void enterOrformula(@NotNull MTLGrammarParser.OrformulaContext ctx);
	/**
	 * Exit a parse tree produced by {@link MTLGrammarParser#orformula}.
	 * @param ctx the parse tree
	 */
	void exitOrformula(@NotNull MTLGrammarParser.OrformulaContext ctx);

	/**
	 * Enter a parse tree produced by {@link MTLGrammarParser#atomic}.
	 * @param ctx the parse tree
	 */
	void enterAtomic(@NotNull MTLGrammarParser.AtomicContext ctx);
	/**
	 * Exit a parse tree produced by {@link MTLGrammarParser#atomic}.
	 * @param ctx the parse tree
	 */
	void exitAtomic(@NotNull MTLGrammarParser.AtomicContext ctx);

	/**
	 * Enter a parse tree produced by {@link MTLGrammarParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterFormula(@NotNull MTLGrammarParser.FormulaContext ctx);
	/**
	 * Exit a parse tree produced by {@link MTLGrammarParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitFormula(@NotNull MTLGrammarParser.FormulaContext ctx);
}