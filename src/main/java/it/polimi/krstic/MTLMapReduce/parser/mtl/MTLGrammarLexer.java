// Generated from MTLGrammar.g4 by ANTLR 4.2.2
package it.polimi.krstic.MTLMapReduce.parser.mtl;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MTLGrammarLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LBRACE=1, RBRACE=2, LSQUARE=3, RSQUARE=4, LPAR=5, RPAR=6, SEMI=7, COLON=8, 
		PLUS=9, MINUS=10, MUL_OP=11, DIV_OP=12, MOD_OP=13, AND_OP=14, OR_OP=15, 
		NOT_OP=16, ASSIGN=17, LT=18, GT=19, EQ=20, NOTEQ=21, LTEQ=22, GTEQ=23, 
		COMMA=24, IMPL=25, EQUV=26, TRUITH=27, FAULTY=28, EXISTS=29, UNTIL=30, 
		RELEASE=31, GLOBALLY=32, NEXT=33, FUTURE=34, SINCE=35, TRIGGER=36, HISTORICALLY=37, 
		YESTERDAY=38, PAST=39, COUNT=40, AVERAGE=41, MAXIMUM=42, PAIRWISE=43, 
		IDENTIFIER=44, NUMBER=45, WS=46, BLOCK_COMMENT=47, LINE_COMMENT=48;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"'{'", "'}'", "'['", "']'", "'('", "')'", "';'", "':'", "'+'", "'-'", 
		"'*'", "'/'", "'%'", "'&'", "'|'", "'!'", "':='", "'<'", "'>'", "'='", 
		"'!='", "'<='", "'>='", "','", "'->'", "'<->'", "'true'", "'false'", "'-E-'", 
		"'-U-'", "'-R-'", "'-G-'", "'-X-'", "'-F-'", "'-S-'", "'-T-'", "'-H-'", 
		"'-Y-'", "'-P-'", "'-C-'", "'-A-'", "'-M-'", "'-D-'", "IDENTIFIER", "NUMBER", 
		"WS", "BLOCK_COMMENT", "LINE_COMMENT"
	};
	public static final String[] ruleNames = {
		"LBRACE", "RBRACE", "LSQUARE", "RSQUARE", "LPAR", "RPAR", "SEMI", "COLON", 
		"PLUS", "MINUS", "MUL_OP", "DIV_OP", "MOD_OP", "AND_OP", "OR_OP", "NOT_OP", 
		"ASSIGN", "LT", "GT", "EQ", "NOTEQ", "LTEQ", "GTEQ", "COMMA", "IMPL", 
		"EQUV", "TRUITH", "FAULTY", "EXISTS", "UNTIL", "RELEASE", "GLOBALLY", 
		"NEXT", "FUTURE", "SINCE", "TRIGGER", "HISTORICALLY", "YESTERDAY", "PAST", 
		"COUNT", "AVERAGE", "MAXIMUM", "PAIRWISE", "IDENTIFIER", "NUMBER", "WS", 
		"BLOCK_COMMENT", "LINE_COMMENT"
	};


	public MTLGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MTLGrammar.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\62\u0111\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\3\2\3\2\3\3\3\3\3\4\3\4"+
		"\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r"+
		"\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\22\3\23\3\23"+
		"\3\24\3\24\3\25\3\25\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\3\31"+
		"\3\31\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3 \3"+
		" \3 \3 \3!\3!\3!\3!\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3$\3$\3$\3$\3%\3%\3%\3"+
		"%\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3)\3)\3)\3)\3*\3*\3*\3*\3+\3"+
		"+\3+\3+\3,\3,\3,\3,\3-\3-\7-\u00e8\n-\f-\16-\u00eb\13-\3.\6.\u00ee\n."+
		"\r.\16.\u00ef\3/\6/\u00f3\n/\r/\16/\u00f4\3/\3/\3\60\3\60\3\60\3\60\7"+
		"\60\u00fd\n\60\f\60\16\60\u0100\13\60\3\60\3\60\3\60\3\60\3\60\3\61\3"+
		"\61\3\61\3\61\7\61\u010b\n\61\f\61\16\61\u010e\13\61\3\61\3\61\3\u00fe"+
		"\2\62\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35"+
		"\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36"+
		";\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62\3\2\7\5\2C\\aac|\6"+
		"\2\62;C\\aac|\3\2\62;\4\2\13\f\"\"\4\2\f\f\17\17\u0115\2\3\3\2\2\2\2\5"+
		"\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2"+
		"\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33"+
		"\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2"+
		"\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2"+
		"\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2"+
		"\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K"+
		"\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2"+
		"\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\3c\3\2\2\2"+
		"\5e\3\2\2\2\7g\3\2\2\2\ti\3\2\2\2\13k\3\2\2\2\rm\3\2\2\2\17o\3\2\2\2\21"+
		"q\3\2\2\2\23s\3\2\2\2\25u\3\2\2\2\27w\3\2\2\2\31y\3\2\2\2\33{\3\2\2\2"+
		"\35}\3\2\2\2\37\177\3\2\2\2!\u0081\3\2\2\2#\u0083\3\2\2\2%\u0086\3\2\2"+
		"\2\'\u0088\3\2\2\2)\u008a\3\2\2\2+\u008c\3\2\2\2-\u008f\3\2\2\2/\u0092"+
		"\3\2\2\2\61\u0095\3\2\2\2\63\u0097\3\2\2\2\65\u009a\3\2\2\2\67\u009e\3"+
		"\2\2\29\u00a3\3\2\2\2;\u00a9\3\2\2\2=\u00ad\3\2\2\2?\u00b1\3\2\2\2A\u00b5"+
		"\3\2\2\2C\u00b9\3\2\2\2E\u00bd\3\2\2\2G\u00c1\3\2\2\2I\u00c5\3\2\2\2K"+
		"\u00c9\3\2\2\2M\u00cd\3\2\2\2O\u00d1\3\2\2\2Q\u00d5\3\2\2\2S\u00d9\3\2"+
		"\2\2U\u00dd\3\2\2\2W\u00e1\3\2\2\2Y\u00e5\3\2\2\2[\u00ed\3\2\2\2]\u00f2"+
		"\3\2\2\2_\u00f8\3\2\2\2a\u0106\3\2\2\2cd\7}\2\2d\4\3\2\2\2ef\7\177\2\2"+
		"f\6\3\2\2\2gh\7]\2\2h\b\3\2\2\2ij\7_\2\2j\n\3\2\2\2kl\7*\2\2l\f\3\2\2"+
		"\2mn\7+\2\2n\16\3\2\2\2op\7=\2\2p\20\3\2\2\2qr\7<\2\2r\22\3\2\2\2st\7"+
		"-\2\2t\24\3\2\2\2uv\7/\2\2v\26\3\2\2\2wx\7,\2\2x\30\3\2\2\2yz\7\61\2\2"+
		"z\32\3\2\2\2{|\7\'\2\2|\34\3\2\2\2}~\7(\2\2~\36\3\2\2\2\177\u0080\7~\2"+
		"\2\u0080 \3\2\2\2\u0081\u0082\7#\2\2\u0082\"\3\2\2\2\u0083\u0084\7<\2"+
		"\2\u0084\u0085\7?\2\2\u0085$\3\2\2\2\u0086\u0087\7>\2\2\u0087&\3\2\2\2"+
		"\u0088\u0089\7@\2\2\u0089(\3\2\2\2\u008a\u008b\7?\2\2\u008b*\3\2\2\2\u008c"+
		"\u008d\7#\2\2\u008d\u008e\7?\2\2\u008e,\3\2\2\2\u008f\u0090\7>\2\2\u0090"+
		"\u0091\7?\2\2\u0091.\3\2\2\2\u0092\u0093\7@\2\2\u0093\u0094\7?\2\2\u0094"+
		"\60\3\2\2\2\u0095\u0096\7.\2\2\u0096\62\3\2\2\2\u0097\u0098\7/\2\2\u0098"+
		"\u0099\7@\2\2\u0099\64\3\2\2\2\u009a\u009b\7>\2\2\u009b\u009c\7/\2\2\u009c"+
		"\u009d\7@\2\2\u009d\66\3\2\2\2\u009e\u009f\7v\2\2\u009f\u00a0\7t\2\2\u00a0"+
		"\u00a1\7w\2\2\u00a1\u00a2\7g\2\2\u00a28\3\2\2\2\u00a3\u00a4\7h\2\2\u00a4"+
		"\u00a5\7c\2\2\u00a5\u00a6\7n\2\2\u00a6\u00a7\7u\2\2\u00a7\u00a8\7g\2\2"+
		"\u00a8:\3\2\2\2\u00a9\u00aa\7/\2\2\u00aa\u00ab\7G\2\2\u00ab\u00ac\7/\2"+
		"\2\u00ac<\3\2\2\2\u00ad\u00ae\7/\2\2\u00ae\u00af\7W\2\2\u00af\u00b0\7"+
		"/\2\2\u00b0>\3\2\2\2\u00b1\u00b2\7/\2\2\u00b2\u00b3\7T\2\2\u00b3\u00b4"+
		"\7/\2\2\u00b4@\3\2\2\2\u00b5\u00b6\7/\2\2\u00b6\u00b7\7I\2\2\u00b7\u00b8"+
		"\7/\2\2\u00b8B\3\2\2\2\u00b9\u00ba\7/\2\2\u00ba\u00bb\7Z\2\2\u00bb\u00bc"+
		"\7/\2\2\u00bcD\3\2\2\2\u00bd\u00be\7/\2\2\u00be\u00bf\7H\2\2\u00bf\u00c0"+
		"\7/\2\2\u00c0F\3\2\2\2\u00c1\u00c2\7/\2\2\u00c2\u00c3\7U\2\2\u00c3\u00c4"+
		"\7/\2\2\u00c4H\3\2\2\2\u00c5\u00c6\7/\2\2\u00c6\u00c7\7V\2\2\u00c7\u00c8"+
		"\7/\2\2\u00c8J\3\2\2\2\u00c9\u00ca\7/\2\2\u00ca\u00cb\7J\2\2\u00cb\u00cc"+
		"\7/\2\2\u00ccL\3\2\2\2\u00cd\u00ce\7/\2\2\u00ce\u00cf\7[\2\2\u00cf\u00d0"+
		"\7/\2\2\u00d0N\3\2\2\2\u00d1\u00d2\7/\2\2\u00d2\u00d3\7R\2\2\u00d3\u00d4"+
		"\7/\2\2\u00d4P\3\2\2\2\u00d5\u00d6\7/\2\2\u00d6\u00d7\7E\2\2\u00d7\u00d8"+
		"\7/\2\2\u00d8R\3\2\2\2\u00d9\u00da\7/\2\2\u00da\u00db\7C\2\2\u00db\u00dc"+
		"\7/\2\2\u00dcT\3\2\2\2\u00dd\u00de\7/\2\2\u00de\u00df\7O\2\2\u00df\u00e0"+
		"\7/\2\2\u00e0V\3\2\2\2\u00e1\u00e2\7/\2\2\u00e2\u00e3\7F\2\2\u00e3\u00e4"+
		"\7/\2\2\u00e4X\3\2\2\2\u00e5\u00e9\t\2\2\2\u00e6\u00e8\t\3\2\2\u00e7\u00e6"+
		"\3\2\2\2\u00e8\u00eb\3\2\2\2\u00e9\u00e7\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea"+
		"Z\3\2\2\2\u00eb\u00e9\3\2\2\2\u00ec\u00ee\t\4\2\2\u00ed\u00ec\3\2\2\2"+
		"\u00ee\u00ef\3\2\2\2\u00ef\u00ed\3\2\2\2\u00ef\u00f0\3\2\2\2\u00f0\\\3"+
		"\2\2\2\u00f1\u00f3\t\5\2\2\u00f2\u00f1\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4"+
		"\u00f2\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f7\b/"+
		"\2\2\u00f7^\3\2\2\2\u00f8\u00f9\7\61\2\2\u00f9\u00fa\7,\2\2\u00fa\u00fe"+
		"\3\2\2\2\u00fb\u00fd\13\2\2\2\u00fc\u00fb\3\2\2\2\u00fd\u0100\3\2\2\2"+
		"\u00fe\u00ff\3\2\2\2\u00fe\u00fc\3\2\2\2\u00ff\u0101\3\2\2\2\u0100\u00fe"+
		"\3\2\2\2\u0101\u0102\7,\2\2\u0102\u0103\7\61\2\2\u0103\u0104\3\2\2\2\u0104"+
		"\u0105\b\60\3\2\u0105`\3\2\2\2\u0106\u0107\7\61\2\2\u0107\u0108\7\61\2"+
		"\2\u0108\u010c\3\2\2\2\u0109\u010b\n\6\2\2\u010a\u0109\3\2\2\2\u010b\u010e"+
		"\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u010f\3\2\2\2\u010e"+
		"\u010c\3\2\2\2\u010f\u0110\b\61\3\2\u0110b\3\2\2\2\b\2\u00e9\u00ef\u00f4"+
		"\u00fe\u010c\4\b\2\2\2\3\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}