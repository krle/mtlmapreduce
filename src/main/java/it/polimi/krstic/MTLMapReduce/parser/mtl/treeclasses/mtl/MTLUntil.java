/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.Until;
import it.polimi.krstic.MTLMapReduce.utils.Printer;

/**
 * @author krle
 *
 */
public class MTLUntil extends Until {

	private Interval I;
	public MTLUntil(Interval i, MTLFormula lfma, MTLFormula rfma) {
		super(lfma, rfma);
		I=i;
		
		Interval withZero = Interval.includeZero(I);
		range=Interval.union(withZero, 
					   Interval.union(Interval.project(withZero, getLeftSub().getRangeInterval()), 
							   		  Interval.project(I, getRightSub().getRangeInterval())));
	}
	@Override
	public String print(Printer p){
		String s="";
		
		s+=getLeftSub().print(p);
		s+=OP;
		s+=getInterval().print();
		s+=getRightSub().print(p);
		
		return p.print(s);
		
	}
	/**
	 * @return the i
	 */
	public Interval getInterval() {
		return I;
	}
	

}
