package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.UnaryFormula;

public class Future extends UnaryFormula {

	public Future(MTLFormula fma) {
		super(fma);
		OP=MTLFormula.FUTURE;
		range = new Interval(0);
	}
	
	@Override
	public boolean future() {
		return true;
	}

}
