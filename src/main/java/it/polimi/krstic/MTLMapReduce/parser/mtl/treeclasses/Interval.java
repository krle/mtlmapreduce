package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses;

public class Interval {
	public boolean infinite=true;

	public long A;

	public Interval(long a){
		A=a;
	}
	public String print() {
		return "["+ A +", +inf)";
	}
	
	public static Interval union(Interval int1, Interval int2){
		if(int1 instanceof BoundedInterval && int2 instanceof BoundedInterval){
			BoundedInterval bint1 = (BoundedInterval) int1;
			BoundedInterval bint2 = (BoundedInterval) int2;
			
			return new BoundedInterval(Math.min(bint1.A, bint2.A),Math.max(bint1.B, bint2.B));
		}
		else
			return new Interval(Math.min(int1.A, int2.A));
	}
	
	public static Interval project(Interval int1, Interval int2){
		if(int1 instanceof BoundedInterval && int2 instanceof BoundedInterval) {
			BoundedInterval bint1 = (BoundedInterval) int1;
			BoundedInterval bint2 = (BoundedInterval) int2;
			
			return new BoundedInterval(bint1.A+bint2.A,bint1.B+bint2.B);
		}
		else
			return new Interval(int1.A+int2.A);
	}
	
	
	public static Interval includeZero(Interval i){
		if(i instanceof BoundedInterval)
			if(((BoundedInterval) i).B>0 && i.A>0)
				return new BoundedInterval(0, ((BoundedInterval) i).B);
			else
				if(((BoundedInterval) i).B<0 && i.A<0)
					return new BoundedInterval(((BoundedInterval) i).B,0);
				else
					return i;
				
		else
			return new Interval(0);
	}
	
	public static Interval negate(Interval i){
		if(i instanceof BoundedInterval)
				return new BoundedInterval(-((BoundedInterval) i).B,-i.A);
		else
			return new Interval(-i.A);
	}
}

