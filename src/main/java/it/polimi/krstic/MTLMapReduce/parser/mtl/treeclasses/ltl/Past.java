/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.UnaryFormula;

/**
 * @author krle
 *
 */
public class Past extends UnaryFormula {

	public Past(MTLFormula fma) {
		super(fma);
		OP=MTLFormula.PAST;
		range = new Interval(0);
	}

}
