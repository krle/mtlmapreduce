package it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl;

import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.Trigger;
import it.polimi.krstic.MTLMapReduce.utils.Printer;

public class MTLTrigger extends Trigger {
	private Interval I;
	public MTLTrigger(Interval i, MTLFormula lfma, MTLFormula rfma) {
		super(lfma, rfma);
		I=i;
		
		Interval neg = Interval.negate(I);
		Interval withZero = Interval.includeZero(neg);
		range=Interval.union(withZero, 
					   Interval.union(Interval.project(withZero, getLeftSub().getRangeInterval()), 
							   		  Interval.project(neg, getRightSub().getRangeInterval())));
	}
	
	@Override
	public String print(Printer p){
		String s="";
		
		s+=getLeftSub().print(p);
		s+=OP;
		s+=I.print();
		s+=getRightSub().print(p);
		
		return p.print(s);

	}
	/**
	 * @return the i
	 */
	public Interval getInterval() {
		return I;
	}
}
