// Generated from MTLGrammar.g4 by ANTLR 4.2.2
package it.polimi.krstic.MTLMapReduce.parser.mtl;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MTLGrammarParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LBRACE=1, RBRACE=2, LSQUARE=3, RSQUARE=4, LPAR=5, RPAR=6, SEMI=7, COLON=8, 
		PLUS=9, MINUS=10, MUL_OP=11, DIV_OP=12, MOD_OP=13, AND_OP=14, OR_OP=15, 
		NOT_OP=16, ASSIGN=17, LT=18, GT=19, EQ=20, NOTEQ=21, LTEQ=22, GTEQ=23, 
		COMMA=24, IMPL=25, EQUV=26, TRUITH=27, FAULTY=28, EXISTS=29, UNTIL=30, 
		RELEASE=31, GLOBALLY=32, NEXT=33, FUTURE=34, SINCE=35, TRIGGER=36, HISTORICALLY=37, 
		YESTERDAY=38, PAST=39, COUNT=40, AVERAGE=41, MAXIMUM=42, PAIRWISE=43, 
		IDENTIFIER=44, NUMBER=45, WS=46, BLOCK_COMMENT=47, LINE_COMMENT=48;
	public static final String[] tokenNames = {
		"<INVALID>", "'{'", "'}'", "'['", "']'", "'('", "')'", "';'", "':'", "'+'", 
		"'-'", "'*'", "'/'", "'%'", "'&'", "'|'", "'!'", "':='", "'<'", "'>'", 
		"'='", "'!='", "'<='", "'>='", "','", "'->'", "'<->'", "'true'", "'false'", 
		"'-E-'", "'-U-'", "'-R-'", "'-G-'", "'-X-'", "'-F-'", "'-S-'", "'-T-'", 
		"'-H-'", "'-Y-'", "'-P-'", "'-C-'", "'-A-'", "'-M-'", "'-D-'", "IDENTIFIER", 
		"NUMBER", "WS", "BLOCK_COMMENT", "LINE_COMMENT"
	};
	public static final int
		RULE_program = 0, RULE_formula = 1, RULE_andformula = 2, RULE_orformula = 3, 
		RULE_atomic = 4, RULE_interval = 5;
	public static final String[] ruleNames = {
		"program", "formula", "andformula", "orformula", "atomic", "interval"
	};

	@Override
	public String getGrammarFileName() { return "MTLGrammar.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MTLGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public FormulaContext formula() {
			return getRuleContext(FormulaContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(12); formula(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormulaContext extends ParserRuleContext {
		public TerminalNode NEXT() { return getToken(MTLGrammarParser.NEXT, 0); }
		public List<FormulaContext> formula() {
			return getRuleContexts(FormulaContext.class);
		}
		public TerminalNode HISTORICALLY() { return getToken(MTLGrammarParser.HISTORICALLY, 0); }
		public TerminalNode LPAR() { return getToken(MTLGrammarParser.LPAR, 0); }
		public TerminalNode GLOBALLY() { return getToken(MTLGrammarParser.GLOBALLY, 0); }
		public TerminalNode YESTERDAY() { return getToken(MTLGrammarParser.YESTERDAY, 0); }
		public TerminalNode OR_OP() { return getToken(MTLGrammarParser.OR_OP, 0); }
		public OrformulaContext orformula() {
			return getRuleContext(OrformulaContext.class,0);
		}
		public TerminalNode IMPL() { return getToken(MTLGrammarParser.IMPL, 0); }
		public FormulaContext formula(int i) {
			return getRuleContext(FormulaContext.class,i);
		}
		public TerminalNode SINCE() { return getToken(MTLGrammarParser.SINCE, 0); }
		public AndformulaContext andformula() {
			return getRuleContext(AndformulaContext.class,0);
		}
		public TerminalNode PAST() { return getToken(MTLGrammarParser.PAST, 0); }
		public TerminalNode TRIGGER() { return getToken(MTLGrammarParser.TRIGGER, 0); }
		public AtomicContext atomic() {
			return getRuleContext(AtomicContext.class,0);
		}
		public TerminalNode AND_OP() { return getToken(MTLGrammarParser.AND_OP, 0); }
		public TerminalNode RELEASE() { return getToken(MTLGrammarParser.RELEASE, 0); }
		public TerminalNode RPAR() { return getToken(MTLGrammarParser.RPAR, 0); }
		public TerminalNode FUTURE() { return getToken(MTLGrammarParser.FUTURE, 0); }
		public TerminalNode NOT_OP() { return getToken(MTLGrammarParser.NOT_OP, 0); }
		public TerminalNode UNTIL() { return getToken(MTLGrammarParser.UNTIL, 0); }
		public TerminalNode EQUV() { return getToken(MTLGrammarParser.EQUV, 0); }
		public IntervalContext interval() {
			return getRuleContext(IntervalContext.class,0);
		}
		public FormulaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formula; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).enterFormula(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).exitFormula(this);
		}
	}

	public final FormulaContext formula() throws RecognitionException {
		return formula(0);
	}

	private FormulaContext formula(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		FormulaContext _localctx = new FormulaContext(_ctx, _parentState);
		FormulaContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_formula, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				{
				setState(15); match(NOT_OP);
				setState(16); formula(26);
				}
				break;

			case 2:
				{
				setState(17); match(GLOBALLY);
				setState(18); formula(19);
				}
				break;

			case 3:
				{
				setState(19); match(NEXT);
				setState(20); formula(18);
				}
				break;

			case 4:
				{
				setState(21); match(FUTURE);
				setState(22); formula(17);
				}
				break;

			case 5:
				{
				setState(23); match(HISTORICALLY);
				setState(24); formula(14);
				}
				break;

			case 6:
				{
				setState(25); match(YESTERDAY);
				setState(26); formula(13);
				}
				break;

			case 7:
				{
				setState(27); match(PAST);
				setState(28); formula(12);
				}
				break;

			case 8:
				{
				setState(29); match(GLOBALLY);
				setState(30); interval();
				setState(31); formula(9);
				}
				break;

			case 9:
				{
				setState(33); match(NEXT);
				setState(34); interval();
				setState(35); formula(8);
				}
				break;

			case 10:
				{
				setState(37); match(FUTURE);
				setState(38); interval();
				setState(39); formula(7);
				}
				break;

			case 11:
				{
				setState(41); match(HISTORICALLY);
				setState(42); interval();
				setState(43); formula(4);
				}
				break;

			case 12:
				{
				setState(45); match(YESTERDAY);
				setState(46); interval();
				setState(47); formula(3);
				}
				break;

			case 13:
				{
				setState(49); match(PAST);
				setState(50); interval();
				setState(51); formula(2);
				}
				break;

			case 14:
				{
				setState(53); atomic();
				}
				break;

			case 15:
				{
				setState(54); match(LPAR);
				setState(55); andformula(0);
				setState(56); match(AND_OP);
				setState(57); formula(0);
				setState(58); match(RPAR);
				}
				break;

			case 16:
				{
				setState(60); match(LPAR);
				setState(61); orformula(0);
				setState(62); match(OR_OP);
				setState(63); formula(0);
				setState(64); match(RPAR);
				}
				break;

			case 17:
				{
				setState(66); match(LPAR);
				setState(67); formula(0);
				setState(68); match(RPAR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(112);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(110);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(72);
						if (!(precpred(_ctx, 23))) throw new FailedPredicateException(this, "precpred(_ctx, 23)");
						setState(73); match(IMPL);
						setState(74); formula(24);
						}
						break;

					case 2:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(75);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(76); match(EQUV);
						setState(77); formula(23);
						}
						break;

					case 3:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(78);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(79); match(UNTIL);
						setState(80); formula(22);
						}
						break;

					case 4:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(81);
						if (!(precpred(_ctx, 20))) throw new FailedPredicateException(this, "precpred(_ctx, 20)");
						setState(82); match(RELEASE);
						setState(83); formula(21);
						}
						break;

					case 5:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(84);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(85); match(SINCE);
						setState(86); formula(17);
						}
						break;

					case 6:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(87);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(88); match(TRIGGER);
						setState(89); formula(16);
						}
						break;

					case 7:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(90);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(91); match(UNTIL);
						setState(92); interval();
						setState(93); formula(12);
						}
						break;

					case 8:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(95);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(96); match(RELEASE);
						setState(97); interval();
						setState(98); formula(11);
						}
						break;

					case 9:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(100);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(101); match(SINCE);
						setState(102); interval();
						setState(103); formula(7);
						}
						break;

					case 10:
						{
						_localctx = new FormulaContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_formula);
						setState(105);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(106); match(TRIGGER);
						setState(107); interval();
						setState(108); formula(6);
						}
						break;
					}
					} 
				}
				setState(114);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AndformulaContext extends ParserRuleContext {
		public FormulaContext formula() {
			return getRuleContext(FormulaContext.class,0);
		}
		public TerminalNode AND_OP() { return getToken(MTLGrammarParser.AND_OP, 0); }
		public AndformulaContext andformula() {
			return getRuleContext(AndformulaContext.class,0);
		}
		public AndformulaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andformula; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).enterAndformula(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).exitAndformula(this);
		}
	}

	public final AndformulaContext andformula() throws RecognitionException {
		return andformula(0);
	}

	private AndformulaContext andformula(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AndformulaContext _localctx = new AndformulaContext(_ctx, _parentState);
		AndformulaContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_andformula, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(116); formula(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(123);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AndformulaContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_andformula);
					setState(118);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(119); match(AND_OP);
					setState(120); formula(0);
					}
					} 
				}
				setState(125);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class OrformulaContext extends ParserRuleContext {
		public FormulaContext formula() {
			return getRuleContext(FormulaContext.class,0);
		}
		public TerminalNode OR_OP() { return getToken(MTLGrammarParser.OR_OP, 0); }
		public OrformulaContext orformula() {
			return getRuleContext(OrformulaContext.class,0);
		}
		public OrformulaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orformula; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).enterOrformula(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).exitOrformula(this);
		}
	}

	public final OrformulaContext orformula() throws RecognitionException {
		return orformula(0);
	}

	private OrformulaContext orformula(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		OrformulaContext _localctx = new OrformulaContext(_ctx, _parentState);
		OrformulaContext _prevctx = _localctx;
		int _startState = 6;
		enterRecursionRule(_localctx, 6, RULE_orformula, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(127); formula(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(134);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new OrformulaContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_orformula);
					setState(129);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(130); match(OR_OP);
					setState(131); formula(0);
					}
					} 
				}
				setState(136);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AtomicContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(MTLGrammarParser.IDENTIFIER, 0); }
		public AtomicContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atomic; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).enterAtomic(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).exitAtomic(this);
		}
	}

	public final AtomicContext atomic() throws RecognitionException {
		AtomicContext _localctx = new AtomicContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_atomic);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137); match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalContext extends ParserRuleContext {
		public TerminalNode RPAR() { return getToken(MTLGrammarParser.RPAR, 0); }
		public TerminalNode LPAR() { return getToken(MTLGrammarParser.LPAR, 0); }
		public TerminalNode RSQUARE() { return getToken(MTLGrammarParser.RSQUARE, 0); }
		public TerminalNode COMMA() { return getToken(MTLGrammarParser.COMMA, 0); }
		public TerminalNode LSQUARE() { return getToken(MTLGrammarParser.LSQUARE, 0); }
		public TerminalNode NUMBER(int i) {
			return getToken(MTLGrammarParser.NUMBER, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(MTLGrammarParser.NUMBER); }
		public IntervalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interval; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).enterInterval(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MTLGrammarListener ) ((MTLGrammarListener)listener).exitInterval(this);
		}
	}

	public final IntervalContext interval() throws RecognitionException {
		IntervalContext _localctx = new IntervalContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_interval);
		try {
			setState(149);
			switch (_input.LA(1)) {
			case LPAR:
				enterOuterAlt(_localctx, 1);
				{
				setState(139); match(LPAR);
				setState(140); match(NUMBER);
				setState(141); match(COMMA);
				setState(142); match(NUMBER);
				setState(143); match(RPAR);
				}
				break;
			case LSQUARE:
				enterOuterAlt(_localctx, 2);
				{
				setState(144); match(LSQUARE);
				setState(145); match(NUMBER);
				setState(146); match(COMMA);
				setState(147); match(NUMBER);
				setState(148); match(RSQUARE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1: return formula_sempred((FormulaContext)_localctx, predIndex);

		case 2: return andformula_sempred((AndformulaContext)_localctx, predIndex);

		case 3: return orformula_sempred((OrformulaContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean andformula_sempred(AndformulaContext _localctx, int predIndex) {
		switch (predIndex) {
		case 10: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean orformula_sempred(OrformulaContext _localctx, int predIndex) {
		switch (predIndex) {
		case 11: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean formula_sempred(FormulaContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 23);

		case 1: return precpred(_ctx, 22);

		case 2: return precpred(_ctx, 21);

		case 3: return precpred(_ctx, 20);

		case 4: return precpred(_ctx, 16);

		case 5: return precpred(_ctx, 15);

		case 6: return precpred(_ctx, 11);

		case 7: return precpred(_ctx, 10);

		case 8: return precpred(_ctx, 6);

		case 9: return precpred(_ctx, 5);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\62\u009a\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3I\n"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\7\3q\n\3\f\3\16\3t\13\3\3\4\3\4\3\4\3\4\3\4\3\4\7\4|\n"+
		"\4\f\4\16\4\177\13\4\3\5\3\5\3\5\3\5\3\5\3\5\7\5\u0087\n\5\f\5\16\5\u008a"+
		"\13\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0098\n\7\3"+
		"\7\2\5\4\6\b\b\2\4\6\b\n\f\2\2\u00b0\2\16\3\2\2\2\4H\3\2\2\2\6u\3\2\2"+
		"\2\b\u0080\3\2\2\2\n\u008b\3\2\2\2\f\u0097\3\2\2\2\16\17\5\4\3\2\17\3"+
		"\3\2\2\2\20\21\b\3\1\2\21\22\7\22\2\2\22I\5\4\3\34\23\24\7\"\2\2\24I\5"+
		"\4\3\25\25\26\7#\2\2\26I\5\4\3\24\27\30\7$\2\2\30I\5\4\3\23\31\32\7\'"+
		"\2\2\32I\5\4\3\20\33\34\7(\2\2\34I\5\4\3\17\35\36\7)\2\2\36I\5\4\3\16"+
		"\37 \7\"\2\2 !\5\f\7\2!\"\5\4\3\13\"I\3\2\2\2#$\7#\2\2$%\5\f\7\2%&\5\4"+
		"\3\n&I\3\2\2\2\'(\7$\2\2()\5\f\7\2)*\5\4\3\t*I\3\2\2\2+,\7\'\2\2,-\5\f"+
		"\7\2-.\5\4\3\6.I\3\2\2\2/\60\7(\2\2\60\61\5\f\7\2\61\62\5\4\3\5\62I\3"+
		"\2\2\2\63\64\7)\2\2\64\65\5\f\7\2\65\66\5\4\3\4\66I\3\2\2\2\67I\5\n\6"+
		"\289\7\7\2\29:\5\6\4\2:;\7\20\2\2;<\5\4\3\2<=\7\b\2\2=I\3\2\2\2>?\7\7"+
		"\2\2?@\5\b\5\2@A\7\21\2\2AB\5\4\3\2BC\7\b\2\2CI\3\2\2\2DE\7\7\2\2EF\5"+
		"\4\3\2FG\7\b\2\2GI\3\2\2\2H\20\3\2\2\2H\23\3\2\2\2H\25\3\2\2\2H\27\3\2"+
		"\2\2H\31\3\2\2\2H\33\3\2\2\2H\35\3\2\2\2H\37\3\2\2\2H#\3\2\2\2H\'\3\2"+
		"\2\2H+\3\2\2\2H/\3\2\2\2H\63\3\2\2\2H\67\3\2\2\2H8\3\2\2\2H>\3\2\2\2H"+
		"D\3\2\2\2Ir\3\2\2\2JK\f\31\2\2KL\7\33\2\2Lq\5\4\3\32MN\f\30\2\2NO\7\34"+
		"\2\2Oq\5\4\3\31PQ\f\27\2\2QR\7 \2\2Rq\5\4\3\30ST\f\26\2\2TU\7!\2\2Uq\5"+
		"\4\3\27VW\f\22\2\2WX\7%\2\2Xq\5\4\3\23YZ\f\21\2\2Z[\7&\2\2[q\5\4\3\22"+
		"\\]\f\r\2\2]^\7 \2\2^_\5\f\7\2_`\5\4\3\16`q\3\2\2\2ab\f\f\2\2bc\7!\2\2"+
		"cd\5\f\7\2de\5\4\3\req\3\2\2\2fg\f\b\2\2gh\7%\2\2hi\5\f\7\2ij\5\4\3\t"+
		"jq\3\2\2\2kl\f\7\2\2lm\7&\2\2mn\5\f\7\2no\5\4\3\boq\3\2\2\2pJ\3\2\2\2"+
		"pM\3\2\2\2pP\3\2\2\2pS\3\2\2\2pV\3\2\2\2pY\3\2\2\2p\\\3\2\2\2pa\3\2\2"+
		"\2pf\3\2\2\2pk\3\2\2\2qt\3\2\2\2rp\3\2\2\2rs\3\2\2\2s\5\3\2\2\2tr\3\2"+
		"\2\2uv\b\4\1\2vw\5\4\3\2w}\3\2\2\2xy\f\3\2\2yz\7\20\2\2z|\5\4\3\2{x\3"+
		"\2\2\2|\177\3\2\2\2}{\3\2\2\2}~\3\2\2\2~\7\3\2\2\2\177}\3\2\2\2\u0080"+
		"\u0081\b\5\1\2\u0081\u0082\5\4\3\2\u0082\u0088\3\2\2\2\u0083\u0084\f\3"+
		"\2\2\u0084\u0085\7\21\2\2\u0085\u0087\5\4\3\2\u0086\u0083\3\2\2\2\u0087"+
		"\u008a\3\2\2\2\u0088\u0086\3\2\2\2\u0088\u0089\3\2\2\2\u0089\t\3\2\2\2"+
		"\u008a\u0088\3\2\2\2\u008b\u008c\7.\2\2\u008c\13\3\2\2\2\u008d\u008e\7"+
		"\7\2\2\u008e\u008f\7/\2\2\u008f\u0090\7\32\2\2\u0090\u0091\7/\2\2\u0091"+
		"\u0098\7\b\2\2\u0092\u0093\7\5\2\2\u0093\u0094\7/\2\2\u0094\u0095\7\32"+
		"\2\2\u0095\u0096\7/\2\2\u0096\u0098\7\6\2\2\u0097\u008d\3\2\2\2\u0097"+
		"\u0092\3\2\2\2\u0098\r\3\2\2\2\bHpr}\u0088\u0097";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}