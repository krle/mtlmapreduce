/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl;


import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.AtomicFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.utils.Debug;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;



/**
 * @author krle
 *
 */
public class MTLParser{

	/* (non-Javadoc)
	 * @see it.polimi.krstic.MTLMapReduce.parser.Parsable#parseFormula(java.lang.String, it.polimi.krstic.MTLMapReduce.parser.Listenable)
	 */
	private MTLListener listener = null;
	private boolean parsed = false;
	
	public void parseFormula(String path) {
		
		listener = new MTLListener();
		
		try{
			
		 	FileInputStream formula =  new FileInputStream(path);
		    //parsing part
		    // create a CharStream that reads from standard input
		    ANTLRInputStream input = new ANTLRInputStream(formula);
		    // create a lexer that feeds off of input CharStream
		    MTLGrammarLexer lexer = new MTLGrammarLexer(input);
		    // create a buffer of tokens pulled from the lexer
		    CommonTokenStream tokens = new CommonTokenStream(lexer);
		    // create a parser that feeds off the tokens buffer
		    MTLGrammarParser parser = new MTLGrammarParser(tokens);
		    // begin parsing at init rule
		    ParseTree tree = parser.program(); 
		    // Create a generic parse tree walker that can trigger callbacks
		    ParseTreeWalker walker = new ParseTreeWalker();
		    // Walk the tree created during the parse, trigger callbacks
		    walker.walk(listener, tree);
		    
		    //pretty print
		    //Debug.print(listener.formula.key());

		    parsed=true;
		}
		catch(Exception e){
			Debug.handleException(e);
		}
		    
		
	}
	
	
	public  void parseString(String stringFma){
		
		listener = new MTLListener();
			
		try {
			ANTLRInputStream input = null;
			ByteArrayInputStream stream = new ByteArrayInputStream(stringFma.getBytes(StandardCharsets.UTF_8));
			input = new ANTLRInputStream(stream);
		    MTLGrammarLexer lexer = new MTLGrammarLexer(input);
		    CommonTokenStream tokens = new CommonTokenStream(lexer);
		    MTLGrammarParser parser = new MTLGrammarParser(tokens);
		    ParseTree tree = parser.program();
		    ParseTreeWalker walker = new ParseTreeWalker();
		    walker.walk(listener, tree);
		    
		    //pretty print
		    //listener.printLatest(new StandardPrinter());

		    parsed=true;
		}
		catch(Exception e){
			Debug.handleException(e);
		}
		
	}
	
	
	public  MTLFormula formula(){
		return listener.formula;
	}

	/**
	 * @return the parsed
	 */
	public  boolean Parsed() {
		return parsed;
	}

	

	public  ArrayList<AtomicFormula> getAtoms() {
		return new ArrayList<AtomicFormula>(listener.atoms.values());
	}
	
	public  HashMap<String,MTLFormula> getMap() {
		return listener.mapping;
	}

	public  void expandFormula(int K){
		listener.expandFormula(K);
	}
	

}
