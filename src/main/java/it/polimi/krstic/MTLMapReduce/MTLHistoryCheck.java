/**
 * 
 */
package it.polimi.krstic.MTLMapReduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.log4j.Logger;

import it.polimi.krstic.MTLMapReduce.mapreduce.*;
import it.polimi.krstic.MTLMapReduce.mapreduce.reader.internal.*;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLParser;
import it.polimi.krstic.MTLMapReduce.utils.Debug;

/**
 * @author krle
 *
 */
public class MTLHistoryCheck {
	
	public static Logger mapLogger =
			Logger.getLogger(MTLMapper.class.getName());
	public static Logger reduceLogger =
			Logger.getLogger(MTLReducer.class.getName());

	private static final int INITTS = 1;
	private static Integer height=1;
	private static String inputFile=null;
	private static String formulaFile=null;
	private static String outputFile=null;
	private static boolean lazy = false;
	private static boolean expand = false;
	private static int K = 0;
	private static MTLParser parser = new MTLParser();

	/**
	 * @param args	
	 */
	public static void main(String[] args){
	    
		//validate arguments
		if (args.length < 3) {
	      System.err.println("USAGE: <jarname>.jar <input history file> <input formula file> <output path> [options]");
	      System.err.println("OPTIONS:");
	      System.err.println("-d debug mode");
	      System.err.println("-l lazy semantics (default: pointwise semantics)");
	      System.err.println("-k N specify interval bound N (only in case of lazy semnatics)");
	      System.exit(-1);
	    }
		
		//parse mandatory params
		inputFile = args[0];
		formulaFile = args[1];
		outputFile = args[2];
		
	    //parse options
		ArrayList<String> options = new ArrayList<String>(Arrays.asList(args));
	    if(options.contains("-d")) {
	    	Debug.activate=true;
	    }
	    
	    if(options.contains("-l")) {	
	    	lazy=true;
	    }
	    
	    if(options.contains("-k")) {
	    	lazy=true;
	    	expand=true;
	    	int i = options.indexOf("-k");
	    	K = Integer.parseInt(options.get(i+1));
	    }
		
	    
	    //parse input formula (and check if successful)
	    
	    parser.parseFormula(formulaFile);
	    if(!parser.Parsed()) {
	    	Debug.print("==========================\n");
	    	Debug.print("  ERROR PARSING FORMULA\n");
	    	Debug.print("==========================\n");
	    	System.exit(-1);
	    }
	    
	    boolean answer = false;
	    try {
			answer=startAlgorithm(inputFile,outputFile);
		} catch (ClassNotFoundException | IOException | InterruptedException e) {
			Debug.handleException(e);
			System.exit(-1);
		}
	    
	    Debug.output("==========================\n");
	    if(answer){
    	Debug.output("Formula "+ parser.formula().key() +" holds \n");
	    }
	    else{
	    	Debug.output("Formula "+ parser.formula().key() +" is violated \n");
	    }
	    Debug.output("==========================\n");

	    System.exit(1);
	    
	}
	
	private static boolean startAlgorithm(String inputFile, String outputFile) throws ClassNotFoundException, IOException, InterruptedException{
	    //main algorithm

		//init
	    Configuration conf=null;
	    int formulaHeight = parser.formula().height();
	    boolean exists = true;
	   
	    //main loop
	    while (height<formulaHeight) {
	    	
	    	//create configuration
	    	conf = new Configuration();
	    	conf.set("iteration", height.toString());
		    conf.set("formula", parser.formula().key());
		    conf.set("debug", String.valueOf(Debug.activate));

		    if(expand){
		    	conf.set("K", String.valueOf(K));
		    }
		    
	    	
		    //MapReduce job
		    Job job = Job.getInstance(conf);
		    job.setJarByClass(MTLHistoryCheck.class);
		    
		    //setup input format
		    if(height==1) {
		    	FileInputFormat.addInputPath(job, new Path(inputFile));
		    	//TODO parametrize the input parser
			    job.setInputFormatClass(GenericInputReader.class);
	    	
		    	SequenceFileOutputFormat.setOutputPath(job, new Path(outputFile+"_1"));
		    	job.setOutputFormatClass(SequenceFileOutputFormat.class);
			   
		    }
		    else {
		    	SequenceFileInputFormat.addInputPath(job, new Path(outputFile+"_"+(height-1)+"/"));
			    job.setInputFormatClass(SequenceFileInputFormat.class);

		    	SequenceFileOutputFormat.setOutputPath(job, new Path(outputFile+"_"+height));
		    	job.setOutputFormatClass(SequenceFileOutputFormat.class);

		    }
		    
		    
		    //set map and reduce classes (based on the semantics)
		    if(!lazy){
			    job.setMapperClass(MTLMapper.class);
			    job.setReducerClass(MTLReducer.class);
		    }
		    else{
		    	job.setMapperClass(LazyMTLMapper.class);
			    job.setReducerClass(LazyMTLReducer.class);
		    }
		    	
		    //setup key/value classes

		    job.setMapOutputKeyClass(CompositeKey.class);
		    job.setMapOutputValueClass(ReduceValue.class);		    
		    job.setOutputKeyClass(Text.class);
		    job.setOutputValueClass(MapValue.class);
		    
		    //secondary sort
		    job.setPartitionerClass(NaturalKeyPartitioner.class);
	        job.setGroupingComparatorClass(NaturalKeyGroupingComparator.class);
	        job.setSortComparatorClass(CompositeKeyComparator.class);
		    
	        //start job
	        job.waitForCompletion(true);
	        
	        //handle intermediate output (check if the file exists)
	        FileSystem FS1=FileSystem.get(conf);
			Path path=new Path(outputFile+"_"+height+"/part-r-00000");
			exists = FS1.exists(path);
			if(exists){
				FileStatus fd = FS1.getFileStatus(path);
				exists = (fd.getLen()>0);
			}
			
			//if it exists and it's nonempty debug
			//otherwise, stop the algorithm
			if(exists){
				SequenceFile.Reader reader = new SequenceFile.Reader(conf,SequenceFile.Reader.file(path));
				Debug.printIntermediateTuples(reader);
			}
			else {
				break;
			}
    
			//increase the iteration number
		    height++;
	    }
	    
	   return analyzeResults(conf);
		
		
	}

	private static boolean analyzeResults(Configuration conf) throws IOException {
		//handle the complete output (check if the file exists)
		boolean exists=false;
	    FileSystem FS1=FileSystem.get(conf);
		Path path=new Path(outputFile+"_"+(height-1)+"/part-r-00000");
		exists = FS1.exists(path);
		if(exists){
			//also nonempty
			FileStatus fd = FS1.getFileStatus(path);
			exists = (fd.getLen()>0);
		}
		
		if(exists){
			
			boolean answer = false;
			SequenceFile.Reader reader = new SequenceFile.Reader(conf,SequenceFile.Reader.file(path));
			Text key = new Text();
	        MapValue value = new MapValue();
	        Debug.print("==========================\n");
        	Debug.print("      Final output\n");
        	Debug.print("==========================\n");
	        while (reader.next(key, value)) {
	        	Debug.printTuple(key.toString(), value);
	        	Debug.print("\n");
	        	answer=answer||(value.getTruith()&&value.getTimestamp()==INITTS);
	        }
	        Debug.print("==========================\n");
	        reader.close();
	        
	        return answer;
			
		}
		else {
			return false;
		}
	}

}
