/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.utils;

/**
 * @author krle
 *
 */
public class StringPrinter implements Printer {

	/* (non-Javadoc)
	 * @see it.polimi.krstic.MTLMapReduce.utils.Printer#print(java.lang.String)
	 */
	@Override
	public String print(String s) {
		return s.trim().replaceAll("\\s+", "");
	}

}
