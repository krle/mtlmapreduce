package it.polimi.krstic.MTLMapReduce.utils;

import java.util.LinkedList;

public class SortedLinkedList{
	
	public LinkedList<Integer> list;
	
	public SortedLinkedList() {
		list= new LinkedList<Integer>();
	}
	
	public Integer get(int index){
		return list.get(index);
	}

	public void addFirst(Integer e){
		list.addFirst(e);
	}
	public void addLast(Integer e){
		list.addLast(e);
	}
	public Integer removeFirst(){
		return list.removeFirst();
	}
	public Integer removeLast(){
		return list.removeLast();
	}
	public void removeAllLast(Integer e){
		while(list.peekLast()!=e)
			list.removeLast();
	}
	public Integer peekFirst(){
		return list.peekFirst();
	}
	public Integer peekLast(){
		return list.peekLast();
	}
	public void clear(){
		list.clear();
	}
	public Integer size(){
		return list.size();
	}
	
	
	public void addUniqueSorted(Integer val){
		
		 if (list.size() == 0) {
	            list.add(val);
	        } else if (list.peekFirst() > val) {
	            list.addFirst(val);
	        } else if (list.peekLast() < val) {
	            list.addLast(val);
	        } else {
	            int i = 0;
	            while (list.get(i) < val) {
	                i++;
	            }
	            if(list.get(i)!=val)
	            	list.add(i, val);
	        }
	}

	public void removeAllFirst(Integer e) {
		while(list.peekFirst()!=e)
			list.removeFirst();
	}
	

}
