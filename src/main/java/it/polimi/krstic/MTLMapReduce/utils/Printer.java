/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.utils;

/**
 * @author krle
 *
 */
public interface Printer {

	public String print(String s); 
}
