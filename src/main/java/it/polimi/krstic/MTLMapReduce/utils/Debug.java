/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.utils;


import java.io.IOException;
import java.util.LinkedList;

import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.hadoop.io.Text;

import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.ReduceValue;

/**
 * @author krle
 *
 */
public class Debug {
	
	public static boolean activate=false;
	//TODO parameterize this field 
	private static Printer printer= new StandardPrinter();

	public static void readerDebug(String key, MapValue value){
		if(activate){
			printer.print("\nREADER:\n");
			printTuple(key,value);
		}
	}
	
	public static void mapperDebug(String key, MapValue value){
		if(activate){
			printer.print("\nMAPPER:\n");
			printer.print("Incoming:\n");
			printTuple(key,value);
		}
	}
	
	public static void mapperDebug(String key, ReduceValue value){
		if(activate){
			printer.print("\nMAPPER:\n");
			printer.print("Outgoing:\n");
			printTuple(key,value);
		}
	}
	
	public static void reducerDebug(String key, ReduceValue value){
		if(activate){
			printer.print("\nREDUCER:\n");
			printer.print("Incoming:\n");
			printTuple(key,value);
		}
	}
	
	public static void reducerDebug(String key, MapValue value){
		if(activate){
			printer.print("\nREDUCER:\n");
			printer.print("Outgoing:\n");
			printTuple(key,value);
		}
	}
	

	

	

	
	

	public static void handleException(Exception e) {
		if(activate){
			printer.print(e.toString());
			if(e.getMessage()!=null){
				printer.print(e.getMessage());
			}
		}
	}
	
	public static void printTuple(String key, MapValue value) {
		if(activate){
			printer.print("<"+key);
			value.print(printer);
			printer.print(">");
		}
	}
	


	public static void print(String str){
		if(activate){
			printer.print(str);
		}
	}

	public static void reducerDebug(LinkedList<ReduceValue> window) {
		if(activate){
			printer.print("\nREDUCER:\n");
			printer.print("Window:\n");
			for (ReduceValue reduceValue : window) {
				reduceValue.print(printer);
			}
		}
	}
	
	
	
	public static void printIntermediateTuples(Reader reader) throws IOException
	{
		if(activate){
	        Text key = new Text();
	        MapValue value = new MapValue();
	        while (reader.next(key, value)) {
	        	printer.print(key.toString());
	        	printer.print(" -> ");
	            value.print(printer);
	            printer.print("\n");
	        } 
		}
		reader.close();
	}

	public static void output(String string) {
		printer.print(string);
	}
	
}
