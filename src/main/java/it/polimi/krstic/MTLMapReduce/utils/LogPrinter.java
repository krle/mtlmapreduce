/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author krle
 *
 */
public class LogPrinter implements Printer {

	/* (non-Javadoc)
	 * @see it.polimi.krstic.MTLMapReduce.utils.Printer#print(java.lang.String)
	 */
	
	private static final Logger logger = LogManager.getLogger("APP");
	
	@Override
	public String print(String s) {
		logger.debug(s);
		return s;
	}

}
