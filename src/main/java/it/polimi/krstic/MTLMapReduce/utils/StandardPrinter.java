package it.polimi.krstic.MTLMapReduce.utils;

public class StandardPrinter implements Printer {

	@Override
	public String print(String s) {
		if(s!=null){
			System.out.print(s);
			return s.trim().replaceAll("\\s+", "");
		}
		return null;
	}

}
