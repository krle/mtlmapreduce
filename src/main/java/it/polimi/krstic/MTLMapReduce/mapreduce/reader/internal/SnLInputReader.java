/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.mapreduce.reader.internal;

import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

/**
 * @author krle
 *
 */
public class SnLInputReader extends FileInputFormat<Text,MapValue> {

	
	 @Override
	 public RecordReader<Text,MapValue> createRecordReader(InputSplit split, TaskAttemptContext context) {
		 return new SnLRecordReader();   
	 }
}
