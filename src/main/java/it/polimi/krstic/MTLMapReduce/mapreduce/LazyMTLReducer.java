package it.polimi.krstic.MTLMapReduce.mapreduce;


import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;

import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLParser;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.AtomicFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BoundedInterval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.bool.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.ltl.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.mtl.*;
import it.polimi.krstic.MTLMapReduce.utils.Debug;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.*;

public class LazyMTLReducer extends Reducer<CompositeKey, ReduceValue, Text, MapValue> {

	private Configuration conf;
	private int iter;
	
	public void reduce(CompositeKey k, Iterable<ReduceValue> val, Context context) 
			throws IOException, InterruptedException{
		
		conf = context.getConfiguration();
		iter=Integer.parseInt(conf.get("iteration"));
		Debug.activate=Boolean.parseBoolean(conf.get("debug"));
		
		
		String strkey = k.key;
		MTLParser parser = new MTLParser();
		parser.parseString(strkey);
		
		MTLFormula key = parser.getMap().get(strkey);
	
		//check the right iteration
		Iterator<ReduceValue> iterator = val.iterator();
		//TODO make the cond more strict
		if(iter!=key.height()-1) {
			reemit(key, iterator,context);
			return;
		}
		
		castAndCallReduce(key, val, context);
	}
	
	private void emit(String key, long ts, boolean v, Context context) 
			throws IOException, InterruptedException{
		MapValue m = new MapValue(ts,v);
		context.write(new Text(key),m); 
		Debug.reducerDebug(key,m);
	}
	
	private void reemit(MTLFormula key, Iterator<ReduceValue> iterator,
			Reducer<CompositeKey, ReduceValue, Text, MapValue>.Context context) throws IOException, InterruptedException {

		while(iterator.hasNext()){
			ReduceValue phi = iterator.next();
			Debug.reducerDebug(key.key(),phi);
			emit(phi.getFormula(), phi.getTimestamp(), phi.getTruith(), context);
			
		}
	}
	

	//Atomic formula (nothing to do here)
 	public void reduceFormula(AtomicFormula key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
	}
	
	//Boolean formulae
	public void reduceFormula(Not key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		for(ReduceValue value : values) {	
			Debug.reducerDebug(key.key(),value);
			emit(key.key(),value.getTimestamp(),!value.getTruith(),context);
		}
		

	}

	public void reduceFormula(And key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		//init
		boolean val = true;
		long Tprev = -1;
		for(ReduceValue value : values) {
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			if(value.getTimestamp()==Tprev) {
				val=val && value.getTruith();
			}
			else {
				if(Tprev!=-1)
					emit(key.key(),Tprev,val,context);
				val = value.getTruith();
			}
			Tprev=value.getTimestamp();
		}
		emit(key.key(),Tprev,val,context);

	}
	
	public void reduceFormula(Or key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		

		//init
		boolean val = false;
		long Tprev = -1;
		
		for(ReduceValue value : values){
			//debug
			Debug.reducerDebug(key.key(),value);

			//apply semantics
			if(value.getTimestamp()==Tprev) {
				val=val || value.getTruith();
			}
			else {
				if(Tprev!=-1)
					emit(key.key(),Tprev,val,context);
				val = value.getTruith();
			}			
			Tprev=value.getTimestamp();
		}
		emit(key.key(),Tprev,val,context);
		
	}
	
	public void reduceFormula(Impl key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		Iterator<ReduceValue> iterator = values.iterator();

		while(iterator.hasNext()){
			//since they are sorted by timestamp these are phi1 and phi2
			//the reducer would not be called if there are no tuples
			ReduceValue phi1 = new ReduceValue(iterator.next());
			ReduceValue phi2 = iterator.next();
			
			//exchange them if necessary
			if(key.getLeftSub().key().equals(phi2.getFormula())){
				ReduceValue tmp = phi1;
				phi1=phi2;
				phi2=tmp;
			}
			
			//debug
			Debug.reducerDebug(key.key(),phi1);
			Debug.reducerDebug(key.key(),phi2);

			//apply semnatics
			emit(key.key(),phi1.getTimestamp(),!phi1.getTruith()||phi2.getTruith(),context);
			
		}
	}
	
	public void reduceFormula(Equiv key, Iterable<ReduceValue> values, Context context)
			throws IOException, InterruptedException{
		
		Iterator<ReduceValue> iterator = values.iterator();
		
		
		

		while(iterator.hasNext()){
			
			//since they are sorted by timestamp these are phi1 and phi2
			//the reducer would not be called if there are no tuples
			ReduceValue phi1 = new ReduceValue(iterator.next());
			ReduceValue phi2 = iterator.next();
			
			//exchange them if necessary
			if(key.getLeftSub().key().equals(phi2.getFormula())){
				ReduceValue tmp = phi1;
				phi1=phi2;
				phi2=tmp;
			}
			
			//debug
			Debug.reducerDebug(key.key(),phi1);
			Debug.reducerDebug(key.key(),phi2);

			//apply semnatics
			emit(key.key(),phi1.getTimestamp(),phi1.getTruith()==phi2.getTruith(),context);
			
		}
	}
		
	//LTL formulae
	public void reduceFormula(Until key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		Iterator<ReduceValue> iterator = values.iterator();
		
		//init
		boolean val = false;

		while(iterator.hasNext()){
			
			//since they are sorted by timestamp these are phi1 and phi2
			ReduceValue phi1 = new ReduceValue(iterator.next());
			ReduceValue phi2 = iterator.next();
			
			//exchange operands
			if(key.getLeftSub().key().equals(phi2.getFormula())){
				ReduceValue tmp = phi1;
				phi1=phi2;
				phi2=tmp;
			}
			//debug
			Debug.reducerDebug(key.key(),phi1);
			Debug.reducerDebug(key.key(),phi2);
			
			//apply semantics
			val=phi2.getTruith() || (val&&phi1.getTruith());
			emit(key.key(), phi1.getTimestamp(), val, context);
			
		}
	}

	public void reduceFormula(Since key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		//TODO copy

	}
	
	public void reduceFormula(Release key, Iterable<ReduceValue> values, Context context){
		//TODO implement
	}
	
	public void reduceFormula(Trigger key, Iterable<ReduceValue> values, Context context){
		//TODO copy
	}
	
	public void reduceFormula(Globally key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		//init
		boolean val = true;

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			val=val && value.getTruith();
			emit(key.key(), value.getTimestamp(), val, context);

		}
		
	}

	public void reduceFormula(Next key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		//init
		boolean val = false;

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			emit(key.key(), value.getTimestamp(), val, context);
			val=value.getTruith();


		}
	}

	public void reduceFormula(Future key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		//init
		boolean val = false;

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			val=val || value.getTruith();
			emit(key.key(), value.getTimestamp(), val, context);

		}
	}

	public void reduceFormula(Historically key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		//init
		boolean val = true;

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			val=val && value.getTruith();
			emit(key.key(), value.getTimestamp(), val, context);

		}
	}

	public void reduceFormula(Yesterday key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		//init
		boolean val = false;

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			emit(key.key(), value.getTimestamp(), val, context);
			val=value.getTruith();

		}
	}

	public void reduceFormula(Past key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		//init
		boolean val = false;

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			val=val || value.getTruith();
			emit(key.key(), value.getTimestamp(), val, context);

		}		
	}
	
	
	//MTL formulae
	private LinkedList<ReduceValue> updateWindow(
			LinkedList<ReduceValue> window, Interval i) {
		
		if(i instanceof BoundedInterval){
			BoundedInterval bi = (BoundedInterval) i;
			while(Math.abs(window.getFirst().getTimestamp()-window.getLast().getTimestamp())>bi.B){
				window.removeFirst();
			}
		}
		return window;
	}
	
	private boolean checkInterval(LinkedList<ReduceValue> window,
			long timestamp, Interval interval) {
		
		for (ReduceValue reduceValue : window) {
			if(Math.abs(timestamp-reduceValue.getTimestamp())>=interval.A)
				return true;
		}
		return false;
	}
	
	public void reduceFormula(MTLUntil key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
	
		Iterator<ReduceValue> iterator = values.iterator();
		
		//init
		boolean val = false;
		boolean metricval = false;
		LinkedList<ReduceValue> window = new LinkedList<ReduceValue>();
		

		while(iterator.hasNext()){
			//since they are sorted by timestamp these are phi1 and phi2
			ReduceValue phi1 = new ReduceValue(iterator.next());
			ReduceValue phi2 = new ReduceValue(iterator.next());

			//exchange operands
			if(key.getLeftSub().key().equals(phi2.getFormula())){
				ReduceValue tmp = phi1;
				phi1=phi2;
				phi2=tmp;
			}
		
			//debug
			Debug.reducerDebug(key.key(),phi1);
			Debug.reducerDebug(key.key(),phi2);
			
			//apply semantics
			if(phi2.getTruith()) {
				window.add(phi2);
				window = updateWindow(window,key.getInterval());
			}
			
			//debug
			Debug.reducerDebug(window);
			
			
			val=phi2.getTruith() || (val&&phi1.getTruith());
			metricval=checkInterval(window,phi1.getTimestamp(),key.getInterval());
			emit(key.key(), phi1.getTimestamp(), val&&metricval, context);
		}
	}
	
	public void reduceFormula(MTLSince key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		//TODO copy
	}
	
	public void reduceFormula(MTLRelease key, Iterable<ReduceValue> values, Context context)
			throws IOException, InterruptedException{
		//TODO implement

	}
	
	public void reduceFormula(MTLTrigger key, Iterable<ReduceValue> values, Context context)
			throws IOException, InterruptedException{
		//TODO copy

	}
	
	public void reduceFormula(MTLGlobally key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		//init
		boolean metricval = false;
		LinkedList<ReduceValue> window = new LinkedList<ReduceValue>();
		

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			if(!value.getTruith()) {
				window.add(new ReduceValue(value));
				window = updateWindow(window,key.getInterval());
			}
			
			//debug
			Debug.reducerDebug(window);
			
			metricval=checkInterval(window,value.getTimestamp(),key.getInterval());
			emit(key.key(), value.getTimestamp(), !metricval, context);
			
		}

	}

	public void reduceFormula(MTLNext key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{

		boolean val = false;
		boolean metricval = false;
		long Tprev=0;

		for(ReduceValue value : values){
			//debug
			Debug.reducerDebug(key.key(), value);
			
			//apply semantics
			long diff=Math.abs(value.getTimestamp()-Tprev);
			if(!key.getInterval().infinite) {
				BoundedInterval bi=(BoundedInterval)key.getInterval();
				metricval=diff>=key.getInterval().A && diff<=bi.B;
			}
			else {
				metricval=Math.abs(value.getTimestamp()-Tprev)>=key.getInterval().A;
			}
			
			emit(key.key(), value.getTimestamp(), val&&metricval, context);
			val=value.getTruith();
			
			Tprev=value.getTimestamp();
		}

	}

	public void reduceFormula(MTLFuture key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		
		//init
		boolean metricval = false;
		LinkedList<ReduceValue> window = new LinkedList<ReduceValue>();
		

		for(ReduceValue value : values){
			
			//debug
			Debug.reducerDebug(key.key(),value);
			
			//apply semantics
			if(value.getTruith()) {
				window.add(new ReduceValue(value));
				window = updateWindow(window,key.getInterval());
			}
			
			//debug
			Debug.reducerDebug(window);
			
			metricval=checkInterval(window,value.getTimestamp(),key.getInterval());
			emit(key.key(), value.getTimestamp(), metricval, context);
			
		}
	}

	public void reduceFormula(MTLHistorically key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		//TODO copy

	}

	public void reduceFormula(MTLYesterday key, Iterable<ReduceValue> values, Context context) 
			throws IOException, InterruptedException{
		//TODO copy

		
	}

	public void reduceFormula(MTLPast key, Iterable<ReduceValue> values, Context context)
			throws IOException, InterruptedException{
		//TODO copy

	}
	
	
	private void castAndCallReduce(MTLFormula key, Iterable<ReduceValue> val,
			Reducer<CompositeKey, ReduceValue, Text, MapValue>.Context context) throws IOException, InterruptedException {
		
		if(key instanceof AtomicFormula){
			reduceFormula((AtomicFormula)key,val,context);
		}
		
		
		if(key instanceof Not){
			reduceFormula((Not)key,val,context);
		}
		if(key instanceof Or){
			reduceFormula((Or)key,val,context);
		}
		if(key instanceof And){
			reduceFormula((And)key,val,context);
		}
		if(key instanceof Impl){
			reduceFormula((Impl)key,val,context);
		}
		if(key instanceof Equiv){
			reduceFormula((Equiv)key,val,context);
		}
		
		
		if(key instanceof MTLUntil){
			reduceFormula((MTLUntil)key,val,context);
		}
		if(key instanceof MTLSince){
			reduceFormula((MTLSince)key,val,context);
		}
		if(key instanceof MTLRelease){
			reduceFormula((MTLRelease)key,val,context);
		}
		if(key instanceof MTLTrigger){
			reduceFormula((MTLTrigger)key,val,context);
		}
		
		if(key instanceof MTLNext){
			reduceFormula((MTLNext)key,val,context);
		}
		if(key instanceof MTLGlobally){
			reduceFormula((MTLGlobally)key,val,context);
		}
		if(key instanceof MTLFuture){
			reduceFormula((MTLFuture)key,val,context);
		}
		
		if(key instanceof MTLYesterday){
			reduceFormula((MTLYesterday)key,val,context);
		}
		if(key instanceof MTLHistorically){
			reduceFormula((MTLHistorically)key,val,context);
		}
		if(key instanceof MTLPast){
			reduceFormula((Past)key,val,context);
		}
		
		
		if(key instanceof Until){
			reduceFormula((Until)key,val,context);
		}
		if(key instanceof Since){
			reduceFormula((Since)key,val,context);
		}
		if(key instanceof Release){
			reduceFormula((Release)key,val,context);
		}
		if(key instanceof Trigger){
			reduceFormula((Trigger)key,val,context);
		}
		
		if(key instanceof Next){
			reduceFormula((Next)key,val,context);
		}
		if(key instanceof Globally){
			reduceFormula((Globally)key,val,context);
		}
		if(key instanceof Future){
			reduceFormula((Future)key,val,context);
		}
		
		if(key instanceof Yesterday){
			reduceFormula((Yesterday)key,val,context);
		}
		if(key instanceof Historically){
			reduceFormula((Historically)key,val,context);
		}
		if(key instanceof Past){
			reduceFormula((Past)key,val,context);
		}
		
		
		


	}
}
