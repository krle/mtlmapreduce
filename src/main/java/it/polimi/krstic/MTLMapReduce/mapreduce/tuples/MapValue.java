package it.polimi.krstic.MTLMapReduce.mapreduce.tuples;

import it.polimi.krstic.MTLMapReduce.utils.Printer;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;


public class MapValue implements  Writable{

	protected LongWritable timestamp = new LongWritable();
	protected BooleanWritable truith = new BooleanWritable();

	public MapValue(){
		
	}
	public MapValue(ReduceValue r) {
		setTimestamp(r.getTimestamp());
		setTruith(r.getTruith());

	}
	public MapValue(long t, boolean b) {
		setTimestamp(t);
		setTruith(b);
		
	}
	/**
	 * @return
	 */
	public long getTimestamp() {
		return timestamp.get();
	}
	/**
	 * @param position
	 */
	public void setTimestamp(long position) {
		this.timestamp.set(position);
	}
	
	/**
	 * @return the truith
	 */
	public boolean getTruith() {
		return truith.get();
	}

	/**
	 * @param truith the truith to set
	 */
	public void setTruith(boolean truith) {
		this.truith.set(truith);
	}

	
	public String print(Printer p) {
		String s = "("+ this.timestamp + " "+ this.truith + ")";
		return p.print(s);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof MapValue){
			MapValue mv = (MapValue) obj;
			return timestamp.get()==mv.getTimestamp() && truith.get()==mv.getTruith();
		}
		return false;
	}

	
	@Override
	public void readFields(DataInput arg0) throws IOException {
		this.timestamp.readFields(arg0);
		this.truith.readFields(arg0);
		
	}
	@Override
	public void write(DataOutput arg0) throws IOException {
		this.timestamp.write(arg0);
		this.truith.write(arg0);
	}
	
	public static MapValue read(DataInput arg0) throws IOException {
		MapValue m = new MapValue();
		m.readFields(arg0);
		return m;
	}

	@Override
	public String toString() {
		return "("+timestamp.get()+ " " + truith.get() +")";
	}
}
