/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.mapreduce.reader.internal;

import java.util.Set;

import it.polimi.krstic.MTLMapReduce.mapreduce.reader.AbstractRecordReader;

/**
 * @author krle
 *
 */
public class InitialRecordReader extends AbstractRecordReader {

	/* (non-Javadoc)
	 * @see it.polimi.krstic.MTLMapReduce.mapreduce.reader.AbstractRecordReader#parseLine(java.lang.String)
	 * write to format of the trace here:
	 * 
	 * (<timestamp>, <pred1>, <pred2>, ... , <predN>)*
	 */
	@Override
	protected boolean parseLine(String check) {

		String[] fields=check.trim().replaceAll("\\s+", "").split(",");
		timestamp=Long.parseLong(fields[0]);
		for (int i = 1; i< fields.length;i++) {
			super.predicates.add(fields[i]);
		}
		return true;
	}
	
	

}
