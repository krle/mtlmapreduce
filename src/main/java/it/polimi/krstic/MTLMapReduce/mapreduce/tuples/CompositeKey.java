package it.polimi.krstic.MTLMapReduce.mapreduce.tuples;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

public class CompositeKey implements WritableComparable<CompositeKey>{
	
	public String key;
	public Long position;
	
	public CompositeKey() {
	}
	public CompositeKey(String s, Long i) {
		key=s;
		position=i;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof CompositeKey){
			CompositeKey ck = (CompositeKey)obj;
			return key.equals(ck.key)&&position.equals(ck.position);
		}
		return false;
		
	}
	@Override
	public void readFields(DataInput arg0) throws IOException {
		key = WritableUtils.readString(arg0);
		position = arg0.readLong();
	}
	@Override
	public void write(DataOutput arg0) throws IOException {
		WritableUtils.writeString(arg0, key);
		arg0.writeLong(position);
	}
	@Override
	public int compareTo(CompositeKey o) {
		int result = key.compareTo(o.key);
		if(0 == result) {
			result = position.compareTo(o.position);
		}
		return result;
	}
	
	@Override
	public String toString() {
		return "("+key+" "+position+")";
	}
}
