package it.polimi.krstic.MTLMapReduce.mapreduce.reader.internal;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.hadoop.io.Text;
import it.polimi.krstic.MTLMapReduce.mapreduce.reader.AbstractRecordReader;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;


public class InternalRecordReader extends AbstractRecordReader {


	@Override
	protected boolean parseLine(String line) throws IOException {
		
		String keyStr = line.toString().substring(0,line.toString().indexOf('\t'));
		String mapStr = line.toString().substring(line.toString().indexOf('\t')+1);
		
		ByteArrayInputStream kbs=new ByteArrayInputStream(keyStr.getBytes());
		ObjectInputStream    kos=new ObjectInputStream(kbs); 		
		String predicate = Text.readString(kos).toString();
		kos.close();
		kbs.close();
		
		ByteArrayInputStream vbs=new ByteArrayInputStream(mapStr.getBytes());
		ObjectInputStream    vos=new ObjectInputStream(vbs);
		
		MapValue m = MapValue.read(vos);
		timestamp = m.getTimestamp();
		boolean truith = m.getTruith();
		
		if(truith)
			predicates.add(predicate);
		else
			negpredicates.add(predicate);
			
		vos.close();
		vbs.close();
		
		return true;
	} 


}
