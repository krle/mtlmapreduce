package it.polimi.krstic.MTLMapReduce.mapreduce.reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.util.LineReader;

import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLParser;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.AtomicFormula;
import it.polimi.krstic.MTLMapReduce.utils.Debug;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;


public abstract class AbstractRecordReader extends RecordReader<Text, MapValue> {

	//reader fields
	private LineReader in;
	private Text key;
	private MapValue value; 
	
	//parsed tuples
	protected Long timestamp=(long) 0;
	protected Set<String> predicates = new HashSet<String>();
	protected Set<String> negpredicates = new HashSet<String>();

	
	//for the percentages
	private long start =0;
    private long end =0;
    private long pos =0;
    
    private MTLParser parser = new MTLParser();
	
	@Override
	public Text getCurrentKey() {

		return key;
	}
	
	@Override
    public MapValue getCurrentValue() throws IOException, InterruptedException {

        return value;
    }

	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapreduce.RecordReader#close()
	 */
	@Override
	public void close() throws IOException {
	}
	
	@Override
	public float getProgress() throws IOException {
		if (start == end) {
            return 0.0f;
        }
        else {
            return Math.min(1.0f, (pos - start) / (float)(end - start));
        }
	}

	@Override
    public void initialize(InputSplit genericSplit, TaskAttemptContext context)throws IOException, InterruptedException {
        FileSplit split = (FileSplit) genericSplit;
        final Path file = split.getPath();
        Configuration conf = context.getConfiguration();
        FileSystem fs = file.getFileSystem(conf);     
        start = split.getStart();
        end= start + split.getLength();
        boolean skipFirstLine = false;
        FSDataInputStream filein = fs.open(split.getPath());
 
        if (start != 0){
            skipFirstLine = true;
            --start;
            filein.seek(start);
        }
        in = new LineReader(filein,conf);
        if(skipFirstLine){
            start += in.readLine(new Text(),0,(int)Math.min((long)Integer.MAX_VALUE, end - start));
        }
        this.pos = start;
        
        
        Debug.activate=Boolean.parseBoolean(conf.get("debug"));
        String formula =conf.get("formula");
        
        
        parser.parseString(formula);
               
    }
	
	@Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        
		
		//check buffered positive tuples
		if(!predicates.isEmpty()){
			emit(predicates);

			return true;
		}
		
		//check buffered negative tuples
		if(!negpredicates.isEmpty()){
			emit(negpredicates);

			return true;
		}
		
		//otherwise
		//read from file
        Text line = new Text();
        int n = in.readLine(line);
        //if something is read
        while(n>0) {
        	
        	//parse it
        	String check = line.toString();
        	boolean succ=false;
        	try{
            	succ=parseLine(check);
        	}
        	catch(IOException e){
        		Debug.handleException(e);
        		clear();

        	    key = null;
        	    value = null; 
        	    return false;
        	}
	
        	if(succ){
        		//if parsing went well there should be something 
            	//in the buffered tuples
   
        		//filter it
        		addNegated();
   
        		//check buffered positive tuples
        		if(!predicates.isEmpty()){
        			emit(predicates);

        			return true;
        		}
        		
        		//or...
        		//check buffered negative tuples
        		if(!negpredicates.isEmpty()){
        			emit(negpredicates);

        			return true;
        		}
        		
        		//otherwise proceed to read the next line of the file
        	}
        	else {
        		//if user signals that something is wrong, clear the buffers
        		//and proceed to read the next line of the file
        		clear();
	    	}
        	n = in.readLine(line);
      }
        
      //end of the file

      key = null;
      value = null; 
      return false;
    }


	/**
	 * clears the buffers
	 */
	private void clear() {
		timestamp=-1l;
		predicates.clear();
		negpredicates.clear();	
	}

	/**
	 * assumes that 'predicates' buffer contains
	 * contains all the positive predicates for the current timestamp
	 * NOTE1: Use only if each line of the file has a unique timestamp
	 * NOTE2: Calls filterAtoms
	 */
	protected void addNegated() {
		filterAtoms();   
		ArrayList<AtomicFormula> atomics = parser.getAtoms();
		for (AtomicFormula atomicFormula : atomics) {
			boolean exists=false;
			for (String s : predicates) {
				if(atomicFormula.key().equals(s)){
					exists=true;
					break;
				}
			}
			if(!exists)
				negpredicates.add(atomicFormula.key());
		}
	}

	/**
	 * @param atomics 
	 */
	protected void  filterAtoms() {
		ArrayList<AtomicFormula> atomics = parser.getAtoms();

		
		Iterator<String> iter = predicates.iterator();

		while (iter.hasNext()) {
			String s = iter.next();
			boolean exists=false;
			for (AtomicFormula atomicFormula : atomics) {
				if(atomicFormula.key().equals(s)){
					exists=true;
					break;
				}
			}
			if(!exists)
				iter.remove();
		}
		
		iter = negpredicates.iterator();
		
		while (iter.hasNext()) {
			String s = iter.next();
			boolean exists=false;
			for (AtomicFormula atomicFormula : atomics) {
				if(atomicFormula.key().equals(s)){
					exists=true;
					break;
				}
			}
			if(!exists)
				iter.remove();
		}
		
	}

	private void emit(Set<String> preds) {
		//emit
		Iterator<String> iter = preds.iterator();
		
		key=new Text(iter.next());
		iter.remove();
		value = new MapValue(timestamp,preds==predicates);         		
		Debug.readerDebug(key.toString(), value);		
	}

	/**
	 * @param check line to be parsed
	 * it takes a line and parses it, extracting predicate(s) and timestamp
	 * into fields atoms and timestamp respecively
	 * @throws IOException 
	 */
	protected abstract boolean parseLine(String check) throws IOException;

 

}
