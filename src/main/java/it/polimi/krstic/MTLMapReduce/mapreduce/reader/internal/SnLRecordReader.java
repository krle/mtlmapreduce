/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.mapreduce.reader.internal;

import java.io.IOException;

import it.polimi.krstic.MTLMapReduce.mapreduce.reader.AbstractRecordReader;

/**
 * @author krle
 *
 */
public class SnLRecordReader extends AbstractRecordReader {

	/* (non-Javadoc)
	 * @see it.polimi.krstic.MTLMapReduce.mapreduce.reader.AbstractRecordReader#parseLine(java.lang.String)
	 */
	@Override
	protected boolean parseLine(String check) throws IOException {
		String[] fields=check.trim().replaceAll("\\s+", "").split(",");
		timestamp=Long.parseLong(fields[fields.length-1]);
		for (int i = 0; i< fields.length-1;i++) {
			super.predicates.add(fields[i]);
		}
		return true;
	}

}
