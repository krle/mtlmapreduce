package it.polimi.krstic.MTLMapReduce.mapreduce.tuples;
import org.apache.hadoop.mapreduce.Partitioner;

public class NaturalKeyPartitioner extends Partitioner<CompositeKey, ReduceValue> {
 
	
	
	
    @Override
    public int getPartition(CompositeKey key, ReduceValue val, int numPartitions) {
        int hash = key.key.hashCode();
        int partition = hash & Integer.MAX_VALUE % numPartitions;
        return partition;
    }
    
 
}


