package it.polimi.krstic.MTLMapReduce.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLParser;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BoundedInterval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.Interval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;
import it.polimi.krstic.MTLMapReduce.utils.Debug;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.CompositeKey;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.ReduceValue;



public class MTLMapper extends Mapper<Text, MapValue, CompositeKey, ReduceValue> {
	


	public void map(Text key, MapValue value, /*[*/Context context/*]*/)
		        throws IOException, /*[*/InterruptedException/*]*/ {
		//get iteration
		Configuration conf = context.getConfiguration();
		int iter=Integer.parseInt(conf.get("iteration"));
		Debug.activate=Boolean.parseBoolean(conf.get("debug"));
		
		//get the formula
		String formula =conf.get("formula");
		MTLParser parser = new MTLParser();
		parser.parseString(formula);
		
		//debug
		Debug.mapperDebug(key.toString(), value);
		
		//consider only relative intervals
		Interval i = parser.formula().getRangeInterval();
		if(i instanceof BoundedInterval)
		{
		
			
			//if the timestamp is outside the relative interval
			if(value.getTimestamp()<i.A || ((BoundedInterval)i).B<value.getTimestamp()) {
				return;
			}
		}
		
		for(MTLFormula parent: parser.getMap().get(key.toString()).getParents()){
			
			if(iter<parent.height()){
				ReduceValue rvalue = new ReduceValue(key.toString(),value);
				
				Debug.mapperDebug(parent.key(), rvalue);
				
				CompositeKey outKey  = new CompositeKey();
				outKey.key= parent.key();
				if(parent.future())
					outKey.position=-rvalue.getTimestamp();
				else
					outKey.position=rvalue.getTimestamp();
			
				context.write(outKey, rvalue);
			}
		}
	}
}
