package it.polimi.krstic.MTLMapReduce.mapreduce.tuples;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import it.polimi.krstic.MTLMapReduce.utils.Printer;



public class ReduceValue extends MapValue implements Writable {


	

	protected Text formula = new Text();
	
	protected ReduceValue() {
	
	}
	public ReduceValue(String f, MapValue m) {
		super(m.getTimestamp(),m.getTruith());
		setFormula(f);
	}
	public ReduceValue(String f,long t, boolean b) {
		super(t,b);
		setFormula(f);
	}
	public ReduceValue(ReduceValue next) {
		super(next.timestamp.get(),next.truith.get());
		setFormula(next.formula.toString());
	}
	public String getFormula() {
		return formula.toString();
	}
	public void setFormula(String formula) {
		this.formula.set(formula);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ReduceValue){
			ReduceValue rv = (ReduceValue) obj;
			return formula.toString().equals(rv.formula.toString()) && super.equals((MapValue)obj);
		}
		return false;
	}

	@Override
	public String print(Printer p) {
		String s = "("+ this.formula + " "+ this.timestamp + " "+ this.truith + ")";
		return p.print(s);
	}
	@Override
	public void readFields(DataInput arg0) throws IOException {
		formula.readFields(arg0);
		super.readFields(arg0);

		
		
	}
	@Override
	public void write(DataOutput arg0) throws IOException {
		formula.write(arg0);
		super.write(arg0);
	}
	
	public static ReduceValue read(DataInput arg0) throws IOException {
		ReduceValue r = new ReduceValue();
		r.readFields(arg0);
		return r;
	}
	
	@Override
	public String toString() {
		return "(" + formula.toString() + " " + super.toString();
	}
}
