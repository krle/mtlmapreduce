package it.polimi.krstic.MTLMapReduce.mapreduce.reader.internal;

import java.io.IOException;
import java.util.StringTokenizer;

import it.polimi.krstic.MTLMapReduce.mapreduce.reader.AbstractRecordReader;

public class GenericRecordReader extends AbstractRecordReader {

	@Override
	protected boolean parseLine(String check) throws IOException {
		StringTokenizer st = new StringTokenizer(check, " ");

		if(st.hasMoreElements()){
			String ts = st.nextElement().toString();
			timestamp = Long.parseLong(ts.trim());
		}
		while(st.hasMoreElements()){
			String pred = st.nextElement().toString();
				predicates.add(pred);
		}
		return true;

	}

}
