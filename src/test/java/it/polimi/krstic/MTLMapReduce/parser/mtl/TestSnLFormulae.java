/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl;

import static org.junit.Assert.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */

public class TestSnLFormulae {

	/**
	 * @throws java.lang.Exception
	 */
	ArrayList<String> fmaStr=new ArrayList<String>();
	ArrayList<MTLFormula> fmas=new ArrayList<MTLFormula>();
	
	@Before
	public void setUp() throws Exception {

		fmaStr.add("-G-a");
		fmaStr.add("-G-!d");
		fmaStr.add("-F-(a&-X--F-a)");
		fmaStr.add("!-F-(c&-X--F-(c&-X--F-(c&-X--F-c)))");
		fmaStr.add("-G-c->-F-[0,1000]d");
		fmaStr.add("-G-a->-F-[1000,1000]b");
		fmaStr.add("-G-a->-F-[0,6000]b");
		fmaStr.add("-G-c->(!-F-[0,99]d&-F-d)");
		fmaStr.add("-G-a->-F-[100,100]b");
		fmaStr.add("-G-a->-F-b->(!-F-[0,999]c->-F-d&-F-c->-F-d)");
		fmaStr.add("-G-a->-F-b->(!-F-[0,999]d&-F-d)");
		fmaStr.add("-G-a->-F-b");



		fmaStr.add("(-F-(b&-P-a)|(-G-!b&-F-a))");
		fmaStr.add("(-F-(b&-X--F-(b&-X--F-(b&-P-(a&-Y--P-a))))|(-G-!-F-(b&-X--F-(b&-X--F-b))&-F-(a&-X--F-a)))");
		fmaStr.add("(-F-(b&-X--F-(b&-H-!d))|-G-(!-F-(b&-X--F-b)&!d))");
		fmaStr.add("(-F-(d&-H-a->-F-[0,3000]c)|-G-(!-F-d&a->-F-[0,3000]c))");
		fmaStr.add("(-F--F-[1000,1000]c->-P-b->(!-F-[0,999]a&-F-a)|-G-b->(!-F-[0,999]a&-F-a))");
		fmaStr.add("(-F-(d&-H-a->-F-[0,6000]b->-F-c)|(-G-!d&a->-F-[0,6000]b->-F-c))");
		fmaStr.add("(-F-(b&-X--F-(b&-X--F-(b&-H-a->(!-F-[0,999]c&-F-c)->-F-d)))|-G-(!-F-(b&-X--F-(b&-X--F-b))&a->(!-F-[0,999]c&-F-c)->-F-d))");
		fmaStr.add("(-F-(d&-H-a->-F-b)|(-G-!d&a->-F-b))");


		fmaStr.add("-F-(a&-F-[0,5000]b)");
		fmaStr.add("-F-(d&-G-c)");
		fmaStr.add("-F-(b&-X--F-(b&-F-[5000,5000]-F-d))");
		fmaStr.add("-F-(a&c->(!-F-[0,999]d&-F-d))");
		fmaStr.add("-F-(a&b->-F-[0,3000]c->-F-d)");
		fmaStr.add("-F-(b&-F-[0,1000]d)");
		fmaStr.add("-F-(b&-X--F-(b&-G-!c))");
		fmaStr.add("-F-(b&-F-[0,6000]c)");
		fmaStr.add("-F-(a&b->-F-[1000,1000]c->-F-[6000,6000]d)");
		fmaStr.add("-F-(b&!-F-(d&-X--F-(d&-X--F-(d&-X--F-(d&-X--F-(d&-X--F-(d&-X--F-d)))))))");
		fmaStr.add("-F-(b&-X--F-(b&-F-[5000,5000]-F-d))");

		fmaStr.add("-G-a->c-U-b");
		fmaStr.add("-G-a->-F-[1000,1000]!c-U--F-[1000,1000]b");

		

	}

	
	
	@Test
	public void test() {
		
		for (String string : fmaStr) {
			MTLParser parser = new MTLParser();
			parser.parseString(string);
			fmas.add(parser.formula());
		}
		
		for (int i =0;i<fmaStr.size();i++) {
			assertEquals(fmaStr.get(i), fmas.get(i).key());			
		}
		
	}

}
