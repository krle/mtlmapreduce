/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl;

import static org.junit.Assert.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */
public class TestMTLFormula {

	private ParseTreeWalker walker;
	private MTLListener listener;
	
	private ParseTree parseString(String testString){
		ANTLRInputStream input = null;
		try {
			ByteArrayInputStream stream = new ByteArrayInputStream(testString.getBytes(StandardCharsets.UTF_8));
			input = new ANTLRInputStream(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    MTLGrammarLexer lexer = new MTLGrammarLexer(input);
	    CommonTokenStream tokens = new CommonTokenStream(lexer);
	    MTLGrammarParser parser = new MTLGrammarParser(tokens);
	    return parser.program(); 
	}
	
	//formulae
	ArrayList<String> strFormulae = new ArrayList<String>();
	ArrayList<MTLFormula> mtlFormulae = new ArrayList<MTLFormula>();
	
	
	//expected results
	ArrayList<Integer> formulaHeight = new ArrayList<Integer>();
	ArrayList<Boolean> atomic = new ArrayList<Boolean>();
	ArrayList<Boolean> temporal = new ArrayList<Boolean>();
	ArrayList<Boolean> hybrid = new ArrayList<Boolean>();

	
	// TODO parse from a file
	@Before
	public void setUp() throws Exception {
		walker =new ParseTreeWalker();
	    listener = new MTLListener();
	    
	    //h=1
		strFormulae.add("p".trim().replaceAll("\\s+", ""));
		strFormulae.add("Add_ds".trim().replaceAll("\\s+", ""));
		strFormulae.add("_p31sd".trim().replaceAll("\\s+", ""));
		strFormulae.add("a1pdsa3".trim().replaceAll("\\s+", ""));
		
		//h=2
		strFormulae.add("! p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-G- p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-X- p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-F- p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-H- p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-Y- p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-P- p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-G- [1,3] p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-X- (1,3) p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-F- [14,33] p".trim().replaceAll("\\s+", ""));
		strFormulae.add("-H- [144,33] p".trim().replaceAll("\\s+", ""));
		strFormulae.add("p -> p".trim().replaceAll("\\s+", ""));
		strFormulae.add("p -> q".trim().replaceAll("\\s+", ""));		
		strFormulae.add("p <-> p".trim().replaceAll("\\s+", ""));
		strFormulae.add("p <-> q".trim().replaceAll("\\s+", ""));
		strFormulae.add("p -U- p".trim().replaceAll("\\s+", ""));
		strFormulae.add("add -R- pds".trim().replaceAll("\\s+", ""));
		strFormulae.add("pd -S- a_p".trim().replaceAll("\\s+", ""));
		strFormulae.add("pd -T- a_p".trim().replaceAll("\\s+", ""));
		strFormulae.add("p -U- [1,3] p".trim().replaceAll("\\s+", ""));
		strFormulae.add("add -R- [4,6] pds".trim().replaceAll("\\s+", ""));
		strFormulae.add("pd -S- (2,8) a_p".trim().replaceAll("\\s+", ""));
		strFormulae.add("pd -T- [2,2] a_p".trim().replaceAll("\\s+", ""));
		strFormulae.add("(p & p & q & f)".trim().replaceAll("\\s+", ""));		
		strFormulae.add("(p | p)".trim().replaceAll("\\s+", ""));
		
		//h=3
		strFormulae.add("(p & (a | d) & (a & g & j) & f)".trim().replaceAll("\\s+", ""));
		strFormulae.add("(p & (a | d) & (a & g & j) & f)".trim().replaceAll("\\s+", ""));
		
		//h=5
		strFormulae.add("(p & (a | -P- -G- j) & (a & g & j) & f)".trim().replaceAll("\\s+", ""));

		
		
		for (String testString : strFormulae) {
			walker.walk(listener, parseString(testString));    
			mtlFormulae.add(listener.formula);
		}
		
		
		//Expected results
		
		//Expected height
		formulaHeight.add(1);
		formulaHeight.add(1);
		formulaHeight.add(1);
		formulaHeight.add(1);
		
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);
		formulaHeight.add(2);

		formulaHeight.add(3);
		formulaHeight.add(3);
		
		formulaHeight.add(5);

		
		//Expected type

			//atomic 
	    atomic.add(true);
	    atomic.add(true);
	    atomic.add(true);
	    atomic.add(true);
	    
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    atomic.add(false);
	    
	    
			//temporal 
	    temporal.add(false);
	    temporal.add(false);
	    temporal.add(false);
	    temporal.add(false);
	    
	    temporal.add(false);
	    
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    
	    temporal.add(false);
	    temporal.add(false);
	    temporal.add(false);
	    temporal.add(false);
	    
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(true);
	    temporal.add(false);
	    temporal.add(false);
	    temporal.add(false);
	    temporal.add(false);
	    temporal.add(false);
	    
	    
			//hybrid 
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    
	    hybrid.add(false);
	    
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    hybrid.add(false);
	    
	}

	@Test
	public void testHeight() {
		
		int i=0;
		for (MTLFormula fma : mtlFormulae) {
			assertEquals(formulaHeight.get(i),new Integer(fma.height()));
			i++;
		}

	}
	
	
	@Test
	public void testType() {
		
		int i=0;
		for (MTLFormula fma : mtlFormulae) {
			assertEquals(atomic.get(i),new Boolean(fma.atomic()));
			assertEquals(temporal.get(i),new Boolean(fma.temporal()));
			assertEquals(hybrid.get(i),new Boolean(fma.hybrid()));
			
			i++;
		}

	}

}
