/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl;

import static org.junit.Assert.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;

import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */
public class TestExpansion {

	/**
	 * @throws java.lang.Exception
	 */
	MTLParser parser;
	
	@Before
	public void setUp() throws Exception {
		parser = new MTLParser();
	}

	@Test
	public void testEventually() {
		String expandedFma="";
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(2);
		expandedFma = parser.formula().key();
		assertEquals("-F-[2,2]-F-[2,2]-F-[2,2]-F-[2,2]-F-[2,2](-F-[0,2]a|-F-[2,2](-F-[0,2]a|-F-[2,2]-F-[0,2]a))", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(3);
		expandedFma = parser.formula().key();
		assertEquals("-F-[3,3]-F-[3,3]-F-[3,3](-F-[1,3]a|-F-[3,3](-F-[0,3]a|-F-[3,3]-F-[0,1]a))", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(4);
		expandedFma = parser.formula().key();
		assertEquals("-F-[4,4]-F-[4,4](-F-[2,4]a|-F-[4,4]-F-[0,4]a)", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(5);
		expandedFma = parser.formula().key();
		assertEquals("-F-[5,5]-F-[5,5](-F-[0,5]a|-F-[5,5]-F-[0,1]a)", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(6);
		expandedFma = parser.formula().key();
		assertEquals("-F-[6,6](-F-[4,6]a|-F-[6,6]-F-[0,4]a)", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(7);
		expandedFma = parser.formula().key();
		assertEquals("-F-[7,7](-F-[3,7]a|-F-[7,7]-F-[0,2]a)", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(12);
		expandedFma = parser.formula().key();
		assertEquals("(-F-[10,12]a|-F-[12,12]-F-[0,4]a)", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(14);
		expandedFma = parser.formula().key();
		assertEquals("(-F-[10,14]a|-F-[14,14]-F-[0,2]a)", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(15);
		expandedFma = parser.formula().key();
		assertEquals("(-F-[10,15]a|-F-[15,15]-F-[0,1]a)", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(16);
		expandedFma = parser.formula().key();
		assertEquals("-F-[10,16]a", expandedFma);
		
		parser.parseString("-F-[10,16]a");
		parser.expandFormula(18);
		expandedFma = parser.formula().key();
		assertEquals("-F-[10,16]a", expandedFma);

	}
	
	public void testMix1() {
		String expandedFma="";
		
		parser.parseString("(-F-[10,16]a | b)");
		parser.expandFormula(2);
		expandedFma = parser.formula().key();
		assertEquals("(-F-[2,2]-F-[2,2]-F-[2,2]-F-[2,2]-F-[2,2](-F-[0,2]a|-F-[2,2](-F-[0,2]a|-F-[2,2]-F-[0,2]a))|b)", expandedFma);
	}

}

