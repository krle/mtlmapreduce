/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl;

import static org.junit.Assert.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.BoundedInterval;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.MTLFormula;

import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */
public class TestRange {

	/**
	 * @throws java.lang.Exception
	 */
	MTLParser parser;
	
	@Before
	public void setUp() throws Exception {
		parser=new MTLParser();
	}

	@Test
	public void testMTLUntilSimple() {
		
		parser.parseString("a-U-[4,7]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,7);
	}
	
	@Test
	public void testMTLEventuallySimple() {
		parser.parseString("-F-[5,8]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,8);
	}
	
	
	@Test
	public void testMTLNextSimple() {
		parser.parseString("-X-[6,9]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,9);
	}
	
	@Test
	public void testMTLGloballySimple() {
		parser.parseString("-G-[7,10]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,10);
	}
	
	@Test
	public void testMTLSinceSimple() {
		parser.parseString("a-S-[4,7]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-7);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	@Test
	public void testMTLPastSimple() {
		parser.parseString("-P-[5,8]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-8);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	
	@Test
	public void testMTLYesterdaySimple() {
		parser.parseString("-Y-[6,9]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-9);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	@Test
	public void testMTLHistoricallySimple() {
		parser.parseString("-H-[7,10]b");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-10);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	
	
	
	@Test
	public void testMTLComplexFuture1() {
		parser.parseString("-G-[7,10]-F-[2,15]a");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,25);
	}
	
	@Test
	public void testMTLComplexFuture2() {
		parser.parseString("-X-[2,12]-G-[2,13]-X-[2,5]-F-[2,15]a");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,45);
	}
	
	@Test
	public void testMTLComplexFuture3() {
		parser.parseString("(-X-[2,12]a) -> -G-[2,13]-X-[2,5]-F-[2,15]a");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,33);
	}
	
	@Test
	public void testMTLComplexFuture4() {
		parser.parseString("(-X-[2,12]a & -G-[2,13]-X-[2,5]-F-[2,15]a)");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,33);
	}
	
	@Test
	public void testMTLComplexFuture5() {
		parser.parseString("(-X-[2,12]a | -G-[2,13]-X-[2,5]-F-[2,15]a)");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,0);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,33);
	}
	
	@Test
	public void testMTLComplexPast1() {
		parser.parseString("-H-[7,10]-P-[2,15]a");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-25);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	@Test
	public void testMTLComplexPast2() {
		parser.parseString("-Y-[2,12]-H-[2,13]-Y-[2,5]-P-[2,15]a");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-45);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	@Test
	public void testMTLComplexPast3() {
		parser.parseString("(-Y-[2,12]a) -> -H-[2,13]-Y-[2,5]-P-[2,15]a");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-33);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	@Test
	public void testMTLComplexPast4() {
		parser.parseString("(-Y-[2,12]a & -H-[2,13]-Y-[2,5]-P-[2,15]a)");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-33);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	@Test
	public void testMTLComplexPast5() {
		parser.parseString("(-Y-[2,12]a | -H-[2,13]-Y-[2,5]-P-[2,15]a)");
		MTLFormula fma = parser.formula();
		assertEquals(fma.getRangeInterval().A,-33);
		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,0);
	}
	
	
	//TODO test mixed intervals
//	@Test
//	public void testMTLMixed1() {
//		parser.parseString("(-X-[2,12]a | -G-[2,13]-Y-[2,5]-P-[2,15]a)");
//		MTLFormula fma = parser.formula();
//		assertEquals(fma.getRangeInterval().A,-20);
//		assertEquals(((BoundedInterval)fma.getRangeInterval()).B,13);
//	}
//	
	
	
	
	

}
