/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.parser.mtl;

import static org.junit.Assert.*;
import it.polimi.krstic.MTLMapReduce.parser.mtl.treeclasses.AtomicFormula;
import it.polimi.krstic.MTLMapReduce.utils.StringPrinter;

import java.io.*;
import java.nio.charset.StandardCharsets;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */

//TODO: refactor the tests to use parser
public class TestParser {

	ParseTreeWalker walker;
	MTLListener listener;
	
	private ParseTree parseString(String testString){
		ANTLRInputStream input = null;
		try {
			ByteArrayInputStream stream = new ByteArrayInputStream(testString.getBytes(StandardCharsets.UTF_8));
			input = new ANTLRInputStream(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    MTLGrammarLexer lexer = new MTLGrammarLexer(input);
	    CommonTokenStream tokens = new CommonTokenStream(lexer);
	    MTLGrammarParser parser = new MTLGrammarParser(tokens);
	    return parser.program(); 
	}
	
	@Before
	public void setUp() throws Exception {
		walker =new ParseTreeWalker();
	    listener = new MTLListener();
	}
	
	@Test
	public void testSpecial() {
		String testString="";
		testString="-G-a".trim().replaceAll("\\s+", "");
		MTLParser parser = new MTLParser();
		parser.parseString(testString);
		assertEquals(parser.getAtoms().isEmpty(),false);
	}
	

	@Test
	public void testAtomic() {
		String testString="";

		testString="p".trim().replaceAll("\\s+", "");
	    walker.walk(listener, parseString(testString));   
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="Add_ds".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="_p31sd".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="a1pdsa3".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
	}
	
	@Test
	public void testUnaryBool() {
		String testString="";

		testString="! p".trim().replaceAll("\\s+", "");
	    walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),(testString));
		

	}
	
	@Test
	public void testUnaryLTL() {
		String testString="";
		
		testString="-G- p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);

		testString="-X- p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="-F- p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="-H- p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="-Y- p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="-P- p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
	}
	
	
	@Test
	public void testUnaryMTL() {
		String testString="";
		
		testString="-G- [1,3] p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
				
		testString="-X- (1,3) p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),"-X-[2,2]p");
		
		testString="-F- [14,33] p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="-H- [144,33] p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="-Y- (132,33) p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),"-Y-[133,32]p");
		
		testString="-P- [1,3] p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
	}
	
	@Test
	public void testBinaryBool() {
		
		String testString="";
		
		testString="p -> p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="p -> q".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="p <-> p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="p <-> q".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
	}
	
	@Test
	public void testBinaryLTL() {
		
		String testString="";
		
		testString="p -U- p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="add -R- pds".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="pd -S- a_p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="pd -T- a_p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
	}
	
	
	@Test
	public void testBinaryMTL() {
		
		String testString="";
		
		testString="p -U- [1,3] p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="add -R- [4,6] pds".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="pd -S- (2,8) a_p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),"pd-S-[3,7]a_p");
		
		testString="pd -T- [2,2] a_p".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
	}
	
	@Test
	public void testNary() {
		String testString="";
		
		testString="(p & p & q & f)".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		testString="(p | p)".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
	}
	
	@Test
	public void testCustom() {
		String testString="";
		
		testString="(p & (a | d) & (a & g & j) & f)".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
			
		testString="(p & (a | d) & (a & g & j) & f)".trim().replaceAll("\\s+", "");
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
		
		testString = "((a -U- b & k ) | f -> -F- g | -G- d <-> ss)".trim().replaceAll("\\s+", "");;
		walker.walk(listener, parseString(testString));    
		assertEquals(listener.printLatest(new StringPrinter()),testString);
		
	}


}
