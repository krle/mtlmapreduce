package it.polimi.krstic.MTLMapReduce.mapreduce;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.krstic.MTLMapReduce.mapreduce.MTLMapper;
import it.polimi.krstic.MTLMapReduce.mapreduce.MTLReducer;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.CompositeKey;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.ReduceValue;
import it.polimi.krstic.MTLMapReduce.utils.Debug;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */

/**
 * @author krle
 *
 */
public class TestMapReduceMTL {

	/**
	 * @throws java.lang.Exception
	 */
	
	MapDriver<Text, MapValue, CompositeKey, ReduceValue> mapDriver;
	ReduceDriver<CompositeKey, ReduceValue, Text, MapValue> reduceDriver;
	MapReduceDriver<Text, MapValue, CompositeKey, ReduceValue, Text, MapValue> mapReduceDriver;
	String fma = "((a-U-[2,3]b & k ) | -F-[2,4]g | -G-[1,2] d | -X-[1,3] h)";
	
	@Before
	public void setUp() throws Exception {
		
		Debug.activate=true;
		
		MTLMapper mapper = new MTLMapper();
		MTLReducer reducer = new MTLReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
		
		Configuration confm = mapDriver.getConfiguration();

		confm.set("iteration","1");
		confm.set("formula",fma);
		confm.set("debug","true");
		
		
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");
		confr.set("formula",fma);
		confr.set("debug","true");
	}

	//Until 
	@Test
	public void testMapperUntil() {
	    mapDriver.addInput(new Text("a"), new MapValue(1,true));
	    mapDriver.addInput(new Text("a"), new MapValue(2,true));
	    mapDriver.addInput(new Text("a"), new MapValue(4,false));
	    mapDriver.addInput(new Text("a"), new MapValue(7,false));
	    
	    mapDriver.addInput(new Text("b"), new MapValue(1,false));
	    mapDriver.addInput(new Text("b"), new MapValue(2,false));
	    mapDriver.addInput(new Text("b"), new MapValue(4,true));
	    mapDriver.addInput(new Text("b"), new MapValue(7,true));

	    
	    mapDriver.addOutput(new CompositeKey("a-U-[2,3]b",-1l), new ReduceValue("a",1,true));
	    mapDriver.addOutput(new CompositeKey("a-U-[2,3]b",-2l), new ReduceValue("a",2,true));
	    mapDriver.addOutput(new CompositeKey("a-U-[2,3]b",-4l), new ReduceValue("a",4,false));
	    mapDriver.addOutput(new CompositeKey("a-U-[2,3]b",-1l), new ReduceValue("b",1,false));
	    mapDriver.addOutput(new CompositeKey("a-U-[2,3]b",-2l), new ReduceValue("b",2,false));
	    mapDriver.addOutput(new CompositeKey("a-U-[2,3]b",-4l), new ReduceValue("b",4,true));
	    
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
 	@Test 
	public void testReducerUntil() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");
		
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	
	   
	   
	   
	    values.add( new ReduceValue("a",7,false));
	    values.add( new ReduceValue("b",7,true));
	    values.add( new ReduceValue("a",4,false));
	    values.add( new ReduceValue("b",4,true));
	    values.add( new ReduceValue("a",2,true));
	    values.add( new ReduceValue("b",2,false));
	    values.add( new ReduceValue("a",1,true));
	    values.add( new ReduceValue("b",1,false));
	    
	    reduceDriver.withInput(new CompositeKey("a-U-[2,3]b",1l), values);
	    
	    
	    reduceDriver.addOutput(new Text("a-U-[2,3]b"), new MapValue(7,false));
	    reduceDriver.addOutput(new Text("a-U-[2,3]b"), new MapValue(4,true));
	    reduceDriver.addOutput(new Text("a-U-[2,3]b"), new MapValue(2,true));
	    reduceDriver.addOutput(new Text("a-U-[2,3]b"), new MapValue(1,true));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }
 	
 	
 	//Eventually 
	@Test
	public void testMapperEventually() {
	    mapDriver.addInput(new Text("g"), new MapValue(1,true));
	    mapDriver.addInput(new Text("g"), new MapValue(2,true));
	    mapDriver.addInput(new Text("g"), new MapValue(4,false));
	    mapDriver.addInput(new Text("g"), new MapValue(7,false));
	    
	  
	    
	    mapDriver.addOutput(new CompositeKey("-F-[2,4]g",-1l), new ReduceValue("g",1,true));
	    mapDriver.addOutput(new CompositeKey("-F-[2,4]g",-2l), new ReduceValue("g",2,true));
	    mapDriver.addOutput(new CompositeKey("-F-[2,4]g",-4l), new ReduceValue("g",4,false));
	   
	    
	    
	    
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
 	@Test 
	public void testReducerEventually() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");
		
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	
	   
	   
	    values.add( new ReduceValue("g",7,false));
	    values.add( new ReduceValue("g",4,false));
	    values.add( new ReduceValue("g",2,true));
	    values.add( new ReduceValue("g",1,true));
	    
	    reduceDriver.withInput(new CompositeKey("-F-[2,4]g",1l), values);
	    
	    
	    reduceDriver.addOutput(new Text("-F-[2,4]g"), new MapValue(7,false));
	    reduceDriver.addOutput(new Text("-F-[2,4]g"), new MapValue(4,false));
	    reduceDriver.addOutput(new Text("-F-[2,4]g"), new MapValue(2,false));
	    reduceDriver.addOutput(new Text("-F-[2,4]g"), new MapValue(1,false));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }

 	
 	//Globally 
	@Test
	public void testMapperGlobally() {
	    mapDriver.addInput(new Text("d"), new MapValue(1,false));
	    mapDriver.addInput(new Text("d"), new MapValue(2,true));
	    mapDriver.addInput(new Text("d"), new MapValue(4,false));
	    mapDriver.addInput(new Text("d"), new MapValue(7,false));
	    
	  
	    
	    mapDriver.addOutput(new CompositeKey("-G-[1,2]d",-1l), new ReduceValue("d",1,false));
	    mapDriver.addOutput(new CompositeKey("-G-[1,2]d",-2l), new ReduceValue("d",2,true));
	    mapDriver.addOutput(new CompositeKey("-G-[1,2]d",-4l), new ReduceValue("d",4,false));
	   
	    
	    
	    
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
 	@Test 
	public void testReducerGlobally() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");
		
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	
	   
	   
	   
	    values.add( new ReduceValue("d",7,false));
	    values.add( new ReduceValue("d",4,false));
	    values.add( new ReduceValue("d",2,true));
	    values.add( new ReduceValue("d",1,false));
	    
	    reduceDriver.withInput(new CompositeKey("-G-[1,2]d",1l), values);
	    
	    
	    reduceDriver.addOutput(new Text("-G-[1,2]d"), new MapValue(7,true));
	    reduceDriver.addOutput(new Text("-G-[1,2]d"), new MapValue(4,true));
	    reduceDriver.addOutput(new Text("-G-[1,2]d"), new MapValue(2,false));
	    reduceDriver.addOutput(new Text("-G-[1,2]d"), new MapValue(1,true));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }
 	
 	//Next 
	@Test
	public void testMapperNext() {
	    mapDriver.addInput(new Text("h"), new MapValue(1,false));
	    mapDriver.addInput(new Text("h"), new MapValue(2,true));
	    mapDriver.addInput(new Text("h"), new MapValue(4,false));
	    mapDriver.addInput(new Text("h"), new MapValue(7,false));
	    
	  
	    
	    mapDriver.addOutput(new CompositeKey("-X-[1,3]h",-1l), new ReduceValue("h",1,false));
	    mapDriver.addOutput(new CompositeKey("-X-[1,3]h",-2l), new ReduceValue("h",2,true));
	    mapDriver.addOutput(new CompositeKey("-X-[1,3]h",-4l), new ReduceValue("h",4,false));
	   
	    
	    
	    
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
 	@Test 
	public void testReducerNext() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");
		
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	
	   
	   
	   
	    values.add( new ReduceValue("h",7,false));
	    values.add( new ReduceValue("h",4,true));
	    values.add( new ReduceValue("h",2,true));
	    values.add( new ReduceValue("h",1,false));
	    
	    reduceDriver.withInput(new CompositeKey("-X-[1,3]h",1l), values);
	    
	    
	    reduceDriver.addOutput(new Text("-X-[1,3]h"), new MapValue(7,false));
	    reduceDriver.addOutput(new Text("-X-[1,3]h"), new MapValue(4,false));
	    reduceDriver.addOutput(new Text("-X-[1,3]h"), new MapValue(2,true));
	    reduceDriver.addOutput(new Text("-X-[1,3]h"), new MapValue(1,true));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }
}
