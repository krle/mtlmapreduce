/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.mapreduce;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.CompositeKey;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.ReduceValue;
import it.polimi.krstic.MTLMapReduce.utils.Debug;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */
public class TestMapReduceLTL {

	/**
	 * @throws java.lang.Exception
	 */
	MapDriver<Text, MapValue, CompositeKey, ReduceValue> mapDriver;
	ReduceDriver<CompositeKey, ReduceValue, Text, MapValue> reduceDriver;
	MapReduceDriver<Text, MapValue, CompositeKey, ReduceValue, Text, MapValue> mapReduceDriver;
	String fma = "((a -U- b & k ) | f -> -F- g | -G- d <-> ss | -X- h)";
	
	
	@Before
	public void setUp() throws Exception {
		
		//Debug.activate=true;
		
		MTLMapper mapper = new MTLMapper();
		MTLReducer reducer = new MTLReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
		
		Configuration confm = mapDriver.getConfiguration();

		confm.set("iteration","1");
		confm.set("formula",fma);
		
		
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");
		confr.set("formula",fma);
		
	}

	//Until
	@Test
	public void testMapperUntil() {
	    mapDriver.withInput(new Text("a"), new MapValue(1,true));
	    mapDriver.withOutput(new CompositeKey("a-U-b",-1l), new ReduceValue("a",1,true));
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
	@Test
	public void testReducerUntil() {
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	    values.add(new ReduceValue("a",1,true));
	    values.add(new ReduceValue("b",1,false));
	    reduceDriver.withInput(new CompositeKey("a-U-b",1l), values);
	    reduceDriver.withOutput(new Text("a-U-b"), new MapValue(1,false));
	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }
	//Eventually
	@Test
	public void testMapperEventually() {
	    mapDriver.withInput(new Text("g"), new MapValue(1,true));
	    mapDriver.withOutput(new CompositeKey("-F-g",-1l), new ReduceValue("g",1,true));
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
	 
	@Test
	public void testReducerEventually() {
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	    values.add(new ReduceValue("g",4,false));
	    values.add(new ReduceValue("g",3,false));
	    values.add(new ReduceValue("g",2,true));
	    values.add(new ReduceValue("g",1,false));
	    reduceDriver.withInput(new CompositeKey("-F-g",1l), values);
	    reduceDriver.addOutput(new Text("-F-g"), new MapValue(4,false));
	    reduceDriver.addOutput(new Text("-F-g"), new MapValue(3,false));
	    reduceDriver.addOutput(new Text("-F-g"), new MapValue(2,true));
	    reduceDriver.addOutput(new Text("-F-g"), new MapValue(1,true));
	    try {
			reduceDriver.runTest();	    	
	    	
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }

	//Next
	@Test
	public void testMapperNext() {
	    mapDriver.addInput(new Text("h"), new MapValue(1,false));
	    mapDriver.addInput(new Text("h"), new MapValue(2,true));
	    mapDriver.addInput(new Text("h"), new MapValue(3,false));
	    mapDriver.addInput(new Text("h"), new MapValue(4,true));

	    
	    mapDriver.addOutput(new CompositeKey("-X-h",-1l), new ReduceValue("h",1,false));
	    mapDriver.addOutput(new CompositeKey("-X-h",-2l), new ReduceValue("h",2,true));
	    mapDriver.addOutput(new CompositeKey("-X-h",-3l), new ReduceValue("h",3,false));
	    mapDriver.addOutput(new CompositeKey("-X-h",-4l), new ReduceValue("h",4,true));
	    
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
	 
	@Test
	public void testReducerNext() {
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	    values.add(new ReduceValue("h",4,true));
	    values.add(new ReduceValue("h",3,false));
	    values.add(new ReduceValue("h",2,true));
	    values.add(new ReduceValue("h",1,false));
	    reduceDriver.withInput(new CompositeKey("-X-h",1l), values);
	    reduceDriver.addOutput(new Text("-X-h"), new MapValue(4,false));
	    reduceDriver.addOutput(new Text("-X-h"), new MapValue(3,true));
	    reduceDriver.addOutput(new Text("-X-h"), new MapValue(2,false));
	    reduceDriver.addOutput(new Text("-X-h"), new MapValue(1,true));
	    try {
			reduceDriver.runTest();	    	
	    	
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }

}
