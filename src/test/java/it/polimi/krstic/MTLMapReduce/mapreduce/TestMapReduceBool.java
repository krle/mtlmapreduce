/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.mapreduce;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.CompositeKey;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.ReduceValue;
import it.polimi.krstic.MTLMapReduce.utils.Debug;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */
public class TestMapReduceBool {

	/**
	 * @throws java.lang.Exception
	 */
	MapDriver<Text, MapValue, CompositeKey, ReduceValue> mapDriver;
	ReduceDriver<CompositeKey, ReduceValue, Text, MapValue> reduceDriver;
	MapReduceDriver<Text, MapValue, CompositeKey, ReduceValue, Text, MapValue> mapReduceDriver;
	String fma = "((a -U- b & k ) | f -> -F- g | -G- d <-> ss | -X- h)";
	
	@Before
	public void setUp() throws Exception {
		
		//Debug.activate=true;
		
		MTLMapper mapper = new MTLMapper();
		MTLReducer reducer = new MTLReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
		
		Configuration confm = mapDriver.getConfiguration();

		confm.set("iteration","1");
		confm.set("formula",fma);
		
		
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");
		confr.set("formula",fma);
	}

	//And 
	@Test
	public void testMapperAnd() {
	    mapDriver.addInput(new Text("a-U-b"), new MapValue(1,true));
	    mapDriver.addInput(new Text("a-U-b"), new MapValue(2,false));
	    mapDriver.addInput(new Text("a-U-b"), new MapValue(3,true));
	    mapDriver.addInput(new Text("a-U-b"), new MapValue(4,false));
	    
	    mapDriver.addInput(new Text("k"), new MapValue(1,true));
	    mapDriver.addInput(new Text("k"), new MapValue(2,false));
	    mapDriver.addInput(new Text("k"), new MapValue(3,false));
	    mapDriver.addInput(new Text("k"), new MapValue(4,true));
	    
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",1l), new ReduceValue("a-U-b",1,true));
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",2l), new ReduceValue("a-U-b",2,false));
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",3l), new ReduceValue("a-U-b",3,true));
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",4l), new ReduceValue("a-U-b",4,false));
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",1l), new ReduceValue("k",1,true));
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",2l), new ReduceValue("k",2,false));
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",3l), new ReduceValue("k",3,false));
	    mapDriver.addOutput(new CompositeKey("(a-U-b&k)",4l), new ReduceValue("k",4,true));
	    
	    
	    
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
 	@Test 
	public void testReducerAnd() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","2");
		
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	    values.add(new ReduceValue("a-U-b",1,true));
	    values.add(new ReduceValue("k",1,true));
	    values.add(new ReduceValue("a-U-b",2,false));
	    values.add(new ReduceValue("k",2,false));
	    values.add(new ReduceValue("a-U-b",3,true));
	    values.add(new ReduceValue("k",3,false));	    
	    values.add(new ReduceValue("a-U-b",4,false));
	    values.add(new ReduceValue("k",4,true));	    
	    reduceDriver.withInput(new CompositeKey("(a-U-b&k)",1l), values);
	    
	    reduceDriver.addOutput(new Text("(a-U-b&k)"), new MapValue(1,true));
	    reduceDriver.addOutput(new Text("(a-U-b&k)"), new MapValue(2,false));
	    reduceDriver.addOutput(new Text("(a-U-b&k)"), new MapValue(3,false));
	    reduceDriver.addOutput(new Text("(a-U-b&k)"), new MapValue(4,false));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }

	
	//Or
	@Test
	public void testMapperOr() {
	    mapDriver.addInput(new Text("(a-U-b&k)"), new MapValue(1,true));
	    mapDriver.addInput(new Text("(a-U-b&k)"), new MapValue(2,false));
	    
	    mapDriver.addInput(new Text("f->-F-g"), new MapValue(1,false));
	    mapDriver.addInput(new Text("f->-F-g"), new MapValue(2,false));
	    
	    mapDriver.addInput(new Text("-G-d<->ss"), new MapValue(1,false));
	    mapDriver.addInput(new Text("-G-d<->ss"), new MapValue(2,false));
	    
	    mapDriver.addInput(new Text("-X-h"), new MapValue(1,true));
	    mapDriver.addInput(new Text("-X-h"), new MapValue(2,false));
	    	    
	    
	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",1l), new ReduceValue("(a-U-b&k)",1,true));
	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",2l), new ReduceValue("(a-U-b&k)",2,false));

	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",1l), new ReduceValue("f->-F-g",1,false));
	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",2l), new ReduceValue("f->-F-g",2,false));

	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",1l), new ReduceValue("-G-d<->ss",1,false));
	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",2l), new ReduceValue("-G-d<->ss",2,false));

	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",1l), new ReduceValue("-X-h",1,true));
	    mapDriver.addOutput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",2l), new ReduceValue("-X-h",2,false));

	    
	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
	@Test 
	public void testReducerOr() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","3");
		
	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	   
	    values.add(new ReduceValue("(a-U-b&k)",1,true));
	    values.add(new ReduceValue("f->-F-g",1,false));
	    values.add(new ReduceValue("-G-d<->ss",1,false));
	    values.add(new ReduceValue("-X-h",1,true));

	    values.add(new ReduceValue("(a-U-b&k)",2,false));
	    values.add(new ReduceValue("f->-F-g",2,false));
	    values.add(new ReduceValue("-G-d<->ss",2,false));
	    values.add(new ReduceValue("-X-h",2,false));
	    
	    reduceDriver.withInput(new CompositeKey("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)",1l), values);
	    
	    reduceDriver.addOutput(new Text("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)"), new MapValue(1,true));
	    reduceDriver.addOutput(new Text("((a-U-b&k)|f->-F-g|-G-d<->ss|-X-h)"), new MapValue(2,false));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }

	//Not
	
	//Impl
	@Test
	public void testMapperImpl() {
	    mapDriver.addInput(new Text("f"), new MapValue(1,true));
	    mapDriver.addInput(new Text("f"), new MapValue(2,false));
	    
	    mapDriver.addInput(new Text("-F-g"), new MapValue(1,false));
	    mapDriver.addInput(new Text("-F-g"), new MapValue(2,true));

	    mapDriver.addOutput(new CompositeKey("f->-F-g",1l), new ReduceValue("f",1,true));
	    mapDriver.addOutput(new CompositeKey("f->-F-g",2l), new ReduceValue("f",2,false));
	    mapDriver.addOutput(new CompositeKey("f->-F-g",1l), new ReduceValue("-F-g",1,false));
	    mapDriver.addOutput(new CompositeKey("f->-F-g",2l), new ReduceValue("-F-g",2,true));


	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
	@Test
	public void testReducerImpl() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","2");

	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	    values.add(new ReduceValue("f",1,true));
	    values.add(new ReduceValue("-F-g",1,false));
	    values.add(new ReduceValue("f",2,false));
	    values.add(new ReduceValue("-F-g",2,true));

	    reduceDriver.withInput(new CompositeKey("f->-F-g",1l), values);
	    
	    reduceDriver.addOutput(new Text("f->-F-g"), new MapValue(1,false));
	    reduceDriver.addOutput(new Text("f->-F-g"), new MapValue(2,true));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }
	
	//Eq
	@Test
	public void testMapperEq() {
	    mapDriver.addInput(new Text("d"), new MapValue(1,true));
	    mapDriver.addInput(new Text("d"), new MapValue(2,false));
	    
	    mapDriver.addInput(new Text("ss"), new MapValue(1,false));
	    mapDriver.addInput(new Text("ss"), new MapValue(2,false));

	    mapDriver.addOutput(new CompositeKey("d<->ss",1l), new ReduceValue("d",1,true));
	    mapDriver.addOutput(new CompositeKey("d<->ss",2l), new ReduceValue("d",2,false));
	    mapDriver.addOutput(new CompositeKey("d<->ss",1l), new ReduceValue("ss",1,false));
	    mapDriver.addOutput(new CompositeKey("d<->ss",2l), new ReduceValue("ss",2,false));


	    try {
	    	mapDriver.runTest();
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	  }
	@Test
	public void testReducerEq() {
		Configuration confr = reduceDriver.getConfiguration();

		confr.set("iteration","1");

	    List<ReduceValue> values = new ArrayList<ReduceValue>();
	    values.add(new ReduceValue("d",1,true));
	    values.add(new ReduceValue("ss",1,false));
	    values.add(new ReduceValue("d",2,false));
	    values.add(new ReduceValue("ss",2,false));

	    reduceDriver.withInput(new CompositeKey("d<->ss",1l), values);
	    
	    reduceDriver.addOutput(new Text("d<->ss"), new MapValue(1,false));
	    reduceDriver.addOutput(new Text("d<->ss"), new MapValue(2,true));

	    try {
			reduceDriver.runTest();
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		}
	 }

}
