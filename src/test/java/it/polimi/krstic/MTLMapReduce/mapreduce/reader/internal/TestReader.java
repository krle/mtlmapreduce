/**
 * 
 */
package it.polimi.krstic.MTLMapReduce.mapreduce.reader.internal;

import static org.junit.Assert.*;
import it.polimi.krstic.MTLMapReduce.mapreduce.tuples.MapValue;
import it.polimi.krstic.MTLMapReduce.parser.mtl.MTLParser;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;
import org.apache.hadoop.util.ReflectionUtils;
import org.junit.Before;
import org.junit.Test;

/**
 * @author krle
 *
 */


//TODO: REFACTOR!!! MOCK!!
public class TestReader {

	/**
	 * @throws java.lang.Exception
	 */
	
	RecordReader<Text, MapValue> rr;
	String fma1 = "((a -U- b & k ) | f -> -F- g | -G- d <-> ss | -X- h)";
	String fma2="(a|b)";
	String file1 = "./Resources/TestFiles/file1";
	String file2 = "./Resources/TestFiles/file2";
	Configuration conf;
	
	@Before
	public void setUp() throws Exception {
		
	
		
		conf = new Configuration();
		conf.set("fs.default.name", "file:///");
		
		
		
		

	}

	
	//Parser
	@Test
	public void testFileParsing1() throws IOException, InterruptedException {
		File testFile = new File(file1);
		Path path = new Path(testFile.getAbsoluteFile().toURI());
		FileSplit split = new FileSplit(path, 0, testFile.length(), null);

		InputFormat<Text, MapValue> inputFormat = ReflectionUtils.newInstance(InitialInputReader.class, conf);
		TaskAttemptContext context = new TaskAttemptContextImpl(conf, new TaskAttemptID());
		rr = inputFormat.createRecordReader(split, context);

		
		MTLParser parser = new MTLParser();
		parser.parseString(fma1);
		rr.initialize(split, context);
		
		
		
		try {
			
			Set<Long> time = new HashSet<Long>();

			while(rr.nextKeyValue()) {
				time.add(rr.getCurrentValue().getTimestamp());
			}
			
			//assertEquals(3, time.size());
			
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		} catch (InterruptedException e) {
			fail("InterruptedException");
			e.printStackTrace();
		}
		
	}
	
	
	@Test
	public void testFileParsing2() throws IOException, InterruptedException {
		File testFile = new File(file2);
		Path path = new Path(testFile.getAbsoluteFile().toURI());
		FileSplit split = new FileSplit(path, 0, testFile.length(), null);

		InputFormat<Text, MapValue> inputFormat = ReflectionUtils.newInstance(InitialInputReader.class, conf);
		TaskAttemptContext context = new TaskAttemptContextImpl(conf, new TaskAttemptID());
		rr = inputFormat.createRecordReader(split, context);
		
		MTLParser parser = new MTLParser();
		parser.parseString(fma2);

		rr.initialize(split, context);
		
		
		try {
			
			
			Set<Long> time = new HashSet<Long>();

			while(rr.nextKeyValue()) {
				time.add(rr.getCurrentValue().getTimestamp());
			}
			
			//assertEquals(8, time.size());
			
			
		} catch (IOException e) {
			fail("IOException");
			e.printStackTrace();
		} catch (InterruptedException e) {
			fail("InterruptedException");
			e.printStackTrace();
		}
		
	}
	
		
	
	
}
