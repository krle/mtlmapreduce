# README #

### Description ###

MTLMapReduce is a distributed and parallel trace checking tool [1] implemented in Java using the Hadoop [2] and the Apache Spark framework [3], which support parallel and distributed computations on large quantities of data. The tool allows the user to check very large traces against formal specifications written in metric temporal logic (MTL) [4].


### Repository description ###

** Branches: **

* Master branch is the implementation of the algorithm using Hadoop 2.6

* Spark branch is the implementation of the algorithm using Spark 1.4.1

** Version: 0.0.1 **


### Setting up the tool ###


**1. Prerequisites**

To build and use the tool you will need Java version >= 1.7, Git version >= 1.9.3 and Maven version >= 3.2.3. 

* To set up Java, refer to this [link](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html).
* To set up Maven, refer to this [link](http://maven.apache.org/install.html)
* To set up Git, refer to this [link](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


**2. Getting the executable**

2.1. Precompiled version: 

* MTLMapReduce precompiled binary can be downloaded from the replication package
* The archive contains four folders: "hadoop", "spark", "scripts" and "examples"
* Folder "scripts" contains the MTL.jar file that is the MTLMapReduce algorithm executable

OR 

2.2. Build MTLMapReduce from the source code:


```
#!bash

git clone https://<yourusername>@bitbucket.org/krle/mtlmapreduce.git
cd mtlmapreduce/
git checkout spark
mvn package

```

the target jar is created at the path:
./target/MTLMapReduce-0.0.1-SNAPSHOT-jar-with-dependencies.jar


**3. Setting up Hadoop and Spark**

3.1. Out-of-the-box Hadoop and Spark deployment:

* MTLMapReduce precompiled binary archive also contains folders "hadoop" and "spark" with Hadoop v2.6 and Spark v1.4.1 deployments.
* set HADOOP_HOME environment variable to refer to the "hadoop" folder
* set SPARK_HOME environment variable to refer to the "spark" folder
* edit ./hadoop/etc/hadoop/hadoop-env.sh file and set
JAVA_HOME to refer to your JRE or JDK (tested with 1.7.0_75-b13)
* run 


```
#!bash

./scripts/hdfs-start.sh 

```

to start HDFS and create your home folder
* you can check if HDFS is working by listing the home folder:


```
#!bash

$HADOOP_HOME/bin/hdfs dfs -ls

```

* run 


```
#!bash

./scripts/start-spark.sh

```

 to start spark and its executors
* you can check if spark is working by opening its web interface on http://localhost:8080/

OR

3.2. Manually set up HADOOP and Spark:

* download and follow the configuration instructions for
[Hadoop](https://hadoop.apache.org/releases.html#Download)
* download and follow the configuration instructions for [Spark](http://spark.apache.org/downloads.html) 
* start HDFS and create your home folder
* start Spark

### Running the tool ###

* to run the algorithm you need to upload the trace and formula files to HDFS
* folder "examples" contains folders "traces" and "formulae" that represent small proof-of-concept examples of traces and G and F formulae
* upload the two folders to hdfs:


```
#!bash

$HADOOP_HOME/bin/hdfs dfs -mkdir traces

$HADOOP_HOME/bin/hdfs dfs -mkdir formulae

$HADOOP_HOME/bin/hdfs dfs -put ./examples/traces/* .

$HADOOP_HOME/bin/hdfs dfs -put ./examples/formulae/* .
```


* go to the folder scripts and execute:


```
#!bash

bash submit.sh traces/trace1 formulae/G/P01 output --reader spark

```

This will check formula -G-[0,50]a ("Atom a is always true in the following 50 time instants") over the first example trace. 

The script submit.sh from the out-of-the-box option has some default settings, you can override them by changing the script file or submitting the spark job yourself:


```
#!bash

$SPARK_HOME/bin/spark-submit \
  --class it.polimi.krstic.MTLMapReduce.SparkHistoryCheck \
  --master spark://localhost:7077 \
  --executor-memory 4g \
  --executor-cores 2 \
  --num-executors 1 \
  MTL.jar  \
  traces/trace1 formulae/G/P01 output --reader spark

```

### MTL syntax ###

You can upload a file with any MTL formula as long as you adhere to the following grammar:


```
#!bash

 <formula> ::= <atom>
 
               || (<formula> & <formula> & ... & <formula>) 

               || (<formula> | <formula> | ... | <formula>)

               || (<formula> -> <formula>)

               || (<formula> <-> <formula>)

               || -F-<formula> || -G-<formula> || -X-<formula> 

               || (<formula>-U-<formula>) || (<formula>-R-<formula>)

               || -P-<formula> || -H-<formula> || -Y-<formula> 

               || (<formula>-S-<formula>) || (<formula>-T-<formula>)

               || -F-<int><formula> || -G-<int><formula> || -X-<int><formula> 

               || (<formula>-U-<int><formula>) || (<formula>-R-<int><formula>)

               || -P-<int><formula> || -H-<int><formula> || -Y-<int><formula> 

               || (<formula>-S-<int><formula>) || (<formula>-T-<int><formula>)

<int> ::= [integer, integer] 

<atom> ::= identifier

```

 
### Replication package ###

The replication package for the experiments in [1] can be found [here](https://figshare.com/articles/ICSE16/5975293)

Crash input for [DecentMon](http://decentmonitor.forge.imag.fr/index.php):
```
#!bash

./decentmon -n 1 -st 1000000 -univ true -dalpha "{p1,p2,p3,p4,p5|p6,p7,p8,p9,p10|p11,p12,p13,p14,p15|p16,p17,p18,p19,p20}" -prt_full true
```



### Who do I talk to? ###

* [Srdan Krstic](http://home.deib.polimi.it/krstic/)

### References ###
[1] Marcello M. Bersani, Domenico Bianculli, Carlo Ghezzi, Srdan Krstic, Pierluigi San Pietro: Efficient Large-scale Trace Checking Using MapReduce [pdf] (http://arxiv.org/abs/1508.06613)

[2] Apache Software Foundation. Hadoop MapReduce. http://hadoop.apache.org/mapreduce/

[3] M. Zaharia, M. Chowdhury, M. J. Franklin, S. Shenker, and I. Stoica. Spark: Cluster computing with working sets

[4] R. Koymans. Specifying real-time properties with metric temporal logic